# clinkup

ClinkUp - advanced databases project - spring 2020
Group 7: Morgan Ludwig, Mitch MacKnight, Matthew Gregory

Project Abstract:
All three of us are joining consulting firms after graduating from Notre Dame where we’ll often be traveling for work. Our future jobs inspired us to create the app ClinkUp which stands for “consultants link up”. On this app, users log their work travel to find out which of their mutual friends and colleagues will also be in the same city at the same time. From here, users can create a “ClinkUp” with another user at a bar or restaurant in the area. Our app suggests restaurants and bars that suit users’ preferences by using the Yelp API. To get more out of the app, users can add friends, view users’ linkedIn profiles, and direct message friends on the app. In addition to a Desktop friendly website, we’ve created a mobile friendly interface that will be easy to use while traveling.

git repo: https://gitlab.com/mgregor4/clinkup/
final video: https://www.youtube.com/watch?v=z3NsZvgbaK8
live website: http://34.206.226.160:8507/
** Note: website will only be live until mid May 2020 **

tech stack:
- Oracle SQL database 
- PHP files for our backend API
- frontend: typescript/Angular
- user interface: Ionic

server setup:
- frontend code served on port 8507
- backend PHP scripts listening on port 8000
- NodeJS server listening on port 8001
- after installing NodeJS, run npm install in the project directory

compile our code:
- inside "clinkup" directory run "ionic build" to compile
- inside "clinkup" directory run "ionic serve" to run in local host
