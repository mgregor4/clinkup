var app = require('express')();
var server = require('http').Server(app);
var port = process.env.PORT || 8001;
var request = require('request');
const message_api = 'http://34.206.226.160:8000/post_message.php'

server.listen(port, function(req,res) {
   console.log('Running server on port ' + port);
});

const io = require('socket.io').listen(server);
// var  middleware = require('socketio-wildcard')();

// io.use(middleware);


// io.configure(function () {
//   io.enable('browser client minification');
//   io.enable('browser client etag');
//   io.enable('browser client gzip');
//   io.set('log level', 1);
//   io.set('transports', ['websocket', 'flashsocket', 'htmlpage', 'xhr-polling', 'jsonp-polling']);
// });

io.on('connection',socket => {
  socket.emit('message','Hello world');
  socket.on('message', message_data => {
    socket.emit('message-mitch',"received");
    send_message(socket,message_data);
  })
});


async function send_message(socket,message_data) {
  try {
    if (!message_data.send_name || !message_data.receive_name || !message_data.text) {
      throw "Insufficient Data";
    }
    var message_post = {"SEND_NAME": message_data.send_name, "RECEIVE_NAME": message_data.receive_name, "TEXT": message_data.text}
    console.log(message_post);
    request.post({
      headers: {'content-type' : 'text/plain'},
      url:     message_api,
      json:    message_post
    },  function (error, response, body) {
      if (error) {
        console.log(`Error accessing message api`,{success: false, error: error, statusCode: response && response.statusCode});
      } else {
        console.log(response.statusCode)
        console.log(response.body)
        socket.emit(`message-${message_data.send_name}`,`Message sent to ${message_data.receive_name}`);
        socket.broadcast.emit(`message-${message_data.receive_name}`, message_data);
      }
    });
  } catch (e) {
    socket.emit('message',e);
    console.log(e);
  }
}

// async function check_pick(socket,message) {
//   socket.emit('message', 'hey');
//
// }
// request.post('https://flaviocopes.com/todos', {
//   json: {
//     todo: 'Buy the milk'
//   }
// }, (error, res, body) => {
//   if (error) {
//     console.error(error)
//     return
//   }
//   console.log(`statusCode: ${res.statusCode}`)
//   console.log(body)
// })
