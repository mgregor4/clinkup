import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Autosize } from './directives/autosize';

@NgModule({
  declarations: [Autosize],
  imports: [
    CommonModule
  ],
  exports: [Autosize]
})
export class DirectivesModule { }
