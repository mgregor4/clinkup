import { Component } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { UserService } from './services/user.service';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'ClinkUps',
      url: '/home/clinkups',
      icon: 'list'
    },
    {
      title: 'Food & Drink',
      url: '/home/food-drink',
      icon: 'beer'
    },
    {
      title: 'Friends',
      url: '/home/friends',
      icon: 'people'
    },
    {
      title: 'Messages',
      url: '/home/messages',
      icon: 'chatbubble-ellipses-sharp'
    },
    {
      title: 'Travel',
      url: '/home/travel',
      icon: 'airplane'
    },
    {
      title: 'Profile',
      url: '/home/profile',
      icon: 'person-circle-outline'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    console.log(this.userService.currentUserValue);
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
