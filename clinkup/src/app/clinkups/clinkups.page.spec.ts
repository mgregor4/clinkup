import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClinkupsPage } from './clinkups.page';

describe('ClinkupsPage', () => {
  let component: ClinkupsPage;
  let fixture: ComponentFixture<ClinkupsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinkupsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClinkupsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
