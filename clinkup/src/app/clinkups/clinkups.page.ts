import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { User } from '../Classes/user';
import { UserService } from '../services/user.service';
import { Travel } from '../Classes/travel';
import { TravelService } from '../services/travel.service';
import { Clinkup } from '../Classes/clinkup';
import { Router } from '@angular/router';
import { Venue } from '../Classes/venue';
import { AlertController } from '@ionic/angular'

@Component({
  selector: 'app-clinkups',
  templateUrl: './clinkups.page.html',
  styleUrls: ['./clinkups.page.scss'],
})
export class ClinkupsPage implements OnInit {

  currentUser: User
  homePage: boolean
  chooseFriend: boolean
  clinkupForm: boolean
  users : any
  start : string
  end : string
  valid : any
  trips : Travel[]
  nextTrip : Travel
  currentTrip: Travel
  clinkups : any
  receive_user_travel: Travel
  receive_user: User
  errorString: string
  goodUsers : any
  tripWithOverlap : Travel
  allClinkups: any
  allClinkupsTemp: any
  public recommendations = []
  public search_results = []
  itemSelected: boolean = false
  venue: any = {}

  ngOnInit() {
  }

  constructor(private apiService: ApiService, private userService: UserService, private travelService : TravelService, private router: Router, public alertController: AlertController) {
    this.errorString = ""
    this.users = ""
    this.nextTrip = new Travel()
    this.currentTrip = new Travel()
    this.tripWithOverlap = new Travel()
    this.clinkups = ""
    this.currentUser = this.userService.currentUserValue
    this.homePage = true
    this.chooseFriend = false
    this.clinkupForm = false
    this.trips = []
    this.goodUsers = []
    this.allClinkups = []

    this.userService.currentUser.subscribe(
      user => {
        this.currentUser = user
        this.get_clinkup()
      }
    )

    this.callGetTravel()
   

  }

  callGetTravel(){
    this.apiService.get_travel(this.currentUser['USERNAME']).subscribe(
      data => {
        this.trips = data

        if (this.trips){
          this.nextTrip = this.travelService.getNextTrip(this.trips)
          this.currentTrip = this.travelService.getCurrentTrip(this.trips)
          let trips = [this.nextTrip, this.currentTrip]
          for (let i =0 ; i < 2; i++){
            let t = trips[i]

            this.goodUsers[i] = []

            if (t['USERNAME']){
              this.apiService.get_travel_from_city(this.currentUser['USERNAME'], t['CITY']).subscribe(
                data => {
                  this.users = data
                  this.start = this.users[0]['START_DATE']
                  this.end = this.users[0]['END_DATE']

                  // check which people cannot clinkup - who is left in this.users can clinkup
                  for (let u of this.users){
                    if (u['USERNAME'] == this.currentUser['USERNAME']){
                      continue
                    }
                    if (u['START_DATE'] > this.end || this.start > u['END_DATE']){
                      continue
                    }
                    this.goodUsers[i].push(u)
                  }
                },
                error => {
                  console.log(error)
                }
              )
            }
          }
        }

      },
      error => {
        console.log(error.status)

      }
    )
  }

  get_clinkup(){
    this.allClinkups = []
    this.apiService.get_clinkup(this.currentUser.USERNAME).subscribe(
      data => {
        if (data) {
          this.allClinkupsTemp = data
          this.allClinkupsTemp.sort((a: { DAY: number; }, b: { DAY: number; }) => (a.DAY > b.DAY) ? 1 : -1)
          for(let clink of this.allClinkupsTemp){
            let newDate = new Date(clink.DAY)
            let today = new Date()
            if (newDate >= today && clink.STATUS != 2){
              this.allClinkups.push(clink)
            }
          }
        }
      },
      error => {
        console.log(error)
      }
    )}

  openNewClinkup(){
    this.callGetTravel()
    this.homePage = false
    this.chooseFriend = true
   
  }

  openClinkupForm(receive_user_travel: Travel, tripWithOverlap: Travel){
    this.tripWithOverlap = tripWithOverlap
    this.receive_user_travel = receive_user_travel
    this.apiService.get_single_user_username(this.receive_user_travel.USERNAME).subscribe(
      data => {
        this.receive_user = data
        this.chooseFriend = false
        this.clinkupForm = true
      },
      error => {
        console.log(error)
      }
    )

  }

  backHome(){
    this.homePage = true
    this.chooseFriend = false
    this.clinkupForm = false
  }

  goToTravel(){
    this.router.navigate(['home/travel'])
  }

  maxDate(date1: string, date2: string){
    let newDate1 = new Date(date1)
    let newDate2 = new Date(date2)
    if (newDate1 > newDate2){
      return date1
    }
    else {
      return date2
    }
  }


  minDate(date1: string, date2: string){
    let newDate1 = new Date(date1)
    let newDate2 = new Date(date2)
    if (newDate1 < newDate2){
      return date1
    }
    else {
      return date2
    }
  }

  pickName(clinkup: Clinkup){
    if (clinkup.SEND_NAME != this.currentUser.USERNAME){
      return clinkup.SEND_NAME
    }
    else{
      return clinkup.RECEIVE_NAME
    }
  }

  submitClinkup(clinkupDate: string){
    this.errorString = ''
    if (!clinkupDate || !this.venue){
      this.errorString = 'all fields are required'
    }
    else{
      let clinkup = new Clinkup()
      clinkup['SEND_NAME'] = this.currentUser.USERNAME
      clinkup['RECEIVE_NAME'] = this.receive_user.USERNAME
      clinkup['DAY'] = clinkupDate
      clinkup['VENUEID'] = this.venue['id']
      clinkup['STATUS'] = 0

      this.apiService.post_clinkup(clinkup).subscribe(
        data => {
          this.get_clinkup()
          this.homePage = true
          this.chooseFriend = false
          this.clinkupForm = false
          this.search_results = []
        },
        error => {
          console.log(error)
        }
      )
    }
  }

  put_clinkup(clinkup: Clinkup, status: number, i: any){
    let updatedClinkup: Clinkup = new Clinkup()
    updatedClinkup.SEND_NAME = clinkup.SEND_NAME
    updatedClinkup.RECEIVE_NAME = clinkup.RECEIVE_NAME
    updatedClinkup.DAY = clinkup.DAY
    updatedClinkup.VENUEID = clinkup.VENUEID
    updatedClinkup.STATUS = status

    this.apiService.put_clinkup(updatedClinkup).subscribe(
      data => {
        this.venue = {}
        this.itemSelected = false
        this.allClinkups[i].STATUS = status
        if (status == 2){
          this.get_clinkup()
        }
      },
      error => {
        console.log(error)
      }
    )

  }

  search_yelp(search_term: string, city: string, state: string) {
    var location = `${city}${state != 'D.C.' ? ', '+state : ''}`
    this.apiService.get_yelp(search_term, location).subscribe(
      data => {
        if (data) {
          this.search_results = [];
          for (let r of data['businesses']) {
            r['categories'] = r['categories'].map( x => x['title']);
            this.search_results.push(r);
          }
        }
      },
      error => {
        console.log(error)
      }
    )
  }

  selectedVenue(id: string, i: any){
    this.venue = this.search_results[i]
    this.search_results = []
    this.itemSelected = true

    let venue: Venue = {
      "VENUEID": this.venue['id'],
      "NAME": this.venue['name'],
      "ADDRESS": this.venue.location.address1,
      "PHOTO_URL": this.venue.image_url
    }
    console.log(venue)
   this.apiService.post_venue(venue).subscribe(
    
     data => {
       console.log('post venue is successful')
     },
     error => {
       console.log(error)
     }
   )

  }

  clinkupStatus(clinkup: Clinkup){
    let returnText: string = ''
    if (clinkup.STATUS==0 && clinkup.SEND_NAME==this.currentUser.USERNAME){
      returnText = 'pending reponse from ' + clinkup.RECEIVE_NAME
    }
    else if (clinkup.STATUS==0 && clinkup.SEND_NAME!=this.currentUser.USERNAME){
      returnText = clinkup.SEND_NAME + ' is waiting on your response'
    }
    else if (clinkup.STATUS==1 && clinkup.SEND_NAME==this.currentUser.USERNAME){
      returnText = clinkup.RECEIVE_NAME + ' accepted your clinkup!'
    }
    else {
      returnText = 'you accepted a clinkup from ' + clinkup.SEND_NAME
    }

    return returnText
  }

  async confirmCancel(clinkup: Clinkup, status: number, i: any){
    let otherName: string
    if (clinkup.SEND_NAME == this.currentUser.USERNAME){
      otherName = clinkup.RECEIVE_NAME
    }
    else {
      otherName = clinkup.SEND_NAME
    }

    const alert = await this.alertController.create({
      header: 'Confirm Cancel',
      message: 'Are you sure you want to cancel this clinkup with ' + otherName + '?',
      buttons: [{
        text: 'Yes',
        handler: () =>{
          this.put_clinkup(clinkup, status, i)
        }
      },{ text: 'No'}]
    })

    await alert.present()
  }

  yelpLink(id: string){
    let link: string = 'http://yelp.com/biz/' + id
    return link
  }

  async refreshRequests(event) {
    await this.get_clinkup()
    event.target.complete();
  }
  

}
