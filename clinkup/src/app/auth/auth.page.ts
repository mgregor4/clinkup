import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { User } from '../Classes/user';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  errorStringLogin: string;
  errorStringRegister: string;
  errorStringSlide1: string;
  errorStringSlide2: string;
  errorStringSlide3: string;
  isLogin: boolean;
  getInfo: boolean;
  slide1: boolean;
  slide2: boolean;
  slide3: boolean;
  user: User;

  constructor(private apiService: ApiService, private userService: UserService,  public router: Router) {
    this.user = new User();
    this.errorStringLogin = '';
    this.errorStringRegister = '';
    this.errorStringSlide1 = '';
    this.errorStringSlide2 = '';
    this.errorStringSlide3 = '';
    this.isLogin = true;
    this.getInfo = false;
    this.slide1 = false;
    this.slide2 = false;
    this.slide3 = false;
   }

  ngOnInit() {
  }

  login(username: string, password: string) {
    this.errorStringLogin = '';
    if (username == ''){
      this.errorStringLogin = 'username required';
    }
    else if (password == ''){
      this.errorStringLogin = 'password required';
    }
    else{
      this.apiService.get_single_user(username, password).subscribe(
        data => {
          console.log(data);
          this.user = data;
          this.userService.login(this.user);
          this.router.navigate(['']);
        },
        error => {
          console.log(error.status);
          if (error.status == 415){
            this.errorStringLogin = 'incorrect password';
          }
          else if (error.status == 410){
            this.errorStringLogin = "user doesn't exist";
          }
          else {
            this.errorStringLogin = 'other login error';
          }

        }
      )
    }
  }

  register(username: string, password: string) {
    this.errorStringRegister = '';
    if (username == ''){
      this.errorStringRegister = 'username required';
    }
    else if (password == ''){
      this.errorStringRegister = 'password required';
    }
    else {
       this.apiService.get_single_user(username, password).subscribe(
        data => {
          this.errorStringRegister = 'user already exists';
        },
        error => {
          console.log(error.status);
          if (error.status == 415){
            this.errorStringRegister = 'user already exists';
          }
          else if (error.status == 410){
            // user doesn't yet exist, now we can allow them to enter profile information
            this.user['USERNAME'] = username;
            this.user['PASSWORD'] = password;
            this.getInfo = true;
            this.slide1 = true;
          }

        }
      )
    }
  }

  submitInfo1(name: string,homeCity: string,homeState: string,linkedIN: string, university: string){
    if (!name || !homeCity || !homeState || !linkedIN || !university){
      this.errorStringSlide1 = 'all fields required';
    }
    else {
      this.user['UNAME'] = name;
      this.user['CITY'] = homeCity;
      this.user['STATE'] = homeState;
      this.user['LINKEDIN'] = linkedIN;
      this.user['UNIVERSITY'] = university;
      this.slide1 = false;
      this.slide2 = true;
    }

  }

  submitInfo2(company: string, bio: string){
    if (!company || !bio){
      this.errorStringSlide2 = 'all fields required';
    }
    else {
      this.user['COMPANY'] = company;
      this.user['BIOGRAPHY'] = bio;

      this.slide2 = false;
      this.slide3 = true;
    }
  }

  submitInfo3(diet: string, food: string, drink: string, messaging: string, searching: string){
    if (!diet || !food || !drink){
      this.errorStringSlide3 = 'all fields required';
    }
    else{
      this.user['DIETARY'] = diet;
      this.user['FOODPREF'] = food;
      this.user['DRINKPREF'] = drink;
      this.user['PRIVACY'] = parseInt(messaging) + parseInt(searching);

      this.apiService.post_user(this.user).subscribe(
        data => {
          // auto login after registration
          this.apiService.get_single_user(this.user['USERNAME'], this.user['PASSWORD']).subscribe(
            data => {
              this.user = data;
              this.userService.login(this.user);
              this.resetScreen();
              this.router.navigate(['']);
            },
            error => {
              console.log(error.status);
            }
          )
        },
        error => {
          console.log(error);
        }
      )
    }

  }

  newUser(){
    this.isLogin = false;
  }

  haveAccount(){
    this.isLogin = true;
  }

  resetScreen(){
    this.isLogin = true;
    this.getInfo = false;
    this.slide1 = false;
    this.slide2 = false;
    this.slide3 = false;
    this.errorStringLogin = '';
    this.errorStringRegister = '';
    this.errorStringSlide1 = '';
    this.errorStringSlide2 = '';
    this.errorStringSlide3 = '';
  }

  fileChanged(event){}

}
