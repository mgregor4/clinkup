import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { DirectivesModule } from '../directives.module';
import { MessagesComponentModule } from '../messages/messages-component.module';
import { ConversationPage } from './conversation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MessagesComponentModule,
    RouterModule.forChild([{ path: '', component: ConversationPage }])
    // MessagesPageModule
  ],
  declarations: [ConversationPage]
})
export class ConversationPageModule {}
