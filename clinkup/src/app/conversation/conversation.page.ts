import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../services/user.service';
import { ApiService } from '../services/api.service';
import { WebSocketService } from '../services/web-socket.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../Classes/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.page.html',
  styleUrls: ['./conversation.page.scss'],
})
export class ConversationPage implements OnInit {

  public conversations = null;
  public messages = [];
  public username: string = null;
  public user: User = new User();
  public listen_sub: Subscription;
  @ViewChild('content', {static: false}) content: any;
  @ViewChild('messagesList', {static: false}) messagesList: any;
  @ViewChild('messageBox', {static: false}) messageBox: any;
  @ViewChild('text', {static: false}) text: any;

  constructor(private userService: UserService, private apiService: ApiService, private websocket: WebSocketService, private router: Router, private route: ActivatedRoute) {
  }

  ionViewWillEnter() {
    this.getAllConverations();
    this.getConversation();
    this.username = this.route.snapshot.paramMap.get('username');
    console.log(this.websocket);
    this.listen_sub = this.websocket.listen(`message-${this.userService.currentUserValue.USERNAME}`).subscribe(
      data => {
        console.log(data);
        this.getConversation();
        this.getAllConverations();
      }
    );
    this.apiService.get_single_user_username(this.username).subscribe(
      data => {
        this.user = data
      },
      error => {
        console.log(error.status)
      }
    );
  }

  ionViewDidEnter() {
    this.content.scrollToBottom();
    setTimeout(() => this.text.el.setFocus());
    this.adjust();
  }

  focusInput() {
    setTimeout(() => this.text.el.setFocus());
  }

  getConversation() {
    this.apiService.get_conversation(this.userService.currentUserValue.USERNAME, this.route.snapshot.paramMap.get('username')).subscribe(
      data => {
        this.messages = data;
        console.log("read");
        this.readMessages();
        this.content.scrollToBottom();
      },
      error => {
        console.log(error);
      }
    )
  }

  sendMessage(text) {
    console.log(text.value);
    if (text.value) {
      var message = {
        "send_name": this.userService.currentUserValue.USERNAME,
        "receive_name": this.route.snapshot.paramMap.get('username'),
        "text": text.value
      };

      this.websocket.emit('message',message);
      text.value = null;
    }
  }

  adjust() {
    console.log( this.messageBox.el.scrollHeight);
    this.messagesList.nativeElement.style.bottom = this.messageBox.el.scrollHeight + 40 + 'px';
    this.messagesList.nativeElement.style.paddingTop = this.messageBox.el.scrollHeight + 'px';
    // console.log(this.element.nativeElement.getElementsByTagName('textarea').length);
    // const textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
    // textArea.style.overflow = 'hidden';
    // textArea.style.height = 'auto';
    // textArea.style.maxHeight = '50px';
    // textArea.style.height = textArea.scrollHeight + 'px';
  }

  viewProfile(username: string) {
    this.router.navigate(['home/messages/profile',username]);
  }

  readMessages() {
    this.apiService.put_message_read(this.route.snapshot.paramMap.get('username'),this.userService.currentUserValue.USERNAME).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

  getAllConverations() {
    this.apiService.get_user_conversations(this.userService.currentUserValue.USERNAME).subscribe(
      data => {
        console.log(data);
        this.conversations = data;
      },
      error => {
        console.log(error);
      }
    )
  }

  ngOnInit() {
  }

  ionViewWillLeave() {
    this.listen_sub.unsubscribe();
  }

}
