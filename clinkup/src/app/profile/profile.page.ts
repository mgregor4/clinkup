import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service'
import { User } from '../Classes/user';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute} from '@angular/router';
import { AlertController } from '@ionic/angular'
import { Friend } from '../Classes/friend';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  pict: string
  image: any
  user : User = new User();
  username : string = null
  link : string
  friendsProfile : boolean
  logoutCheck : boolean
  currentUser: User
  friendStatus: Friend
  showAddFriend: boolean
  showFriendship: boolean
  friendButtonText: string
  acceptRequest : boolean
  acceptText : string
  pendingText : string
  pendingRequest : boolean
  photoProfile: string

  constructor(private apiService: ApiService, private userService : UserService,
    private router : Router, private route : ActivatedRoute, public alertController : AlertController) {}

  ionViewWillEnter() {
    this.pict = ""
    this.image =""
    this.link = "https://www.linkedin.com/in/"
    this.username = this.route.snapshot.paramMap.get('username');
    this.user = new User()
    this.friendsProfile = false
    this.logoutCheck = false
    this.currentUser = new User()
    this.friendStatus = new Friend()
    this.showAddFriend = false
    this.showFriendship = false
    this.friendButtonText = 'send request'
    this.acceptRequest = false
    this.acceptText = "Accept Request?"
    this.pendingRequest = false
    this.pendingText = ""
    this.photoProfile ='../assets/profile.png'

    if (this.router.url.includes('friends')){
      this.friendsProfile = true
      this.currentUser = this.userService.currentUserValue


      if (this.username){
        this.apiService.get_single_user_username(this.username).subscribe(
          data => {
            this.user = data
            this.photoProfile = '../assets/' + this.user.USERNAME + '.jpeg'
            this.link += this.user['LINKEDIN']
            console.log(data)
            this.apiService.get_friend_status(this.username, this.currentUser.USERNAME).subscribe(
              data => {
                console.log(data)
                this.friendStatus = data
                this.processFriendStatus(this.friendStatus)
              },
              error => {
                console.log(error.status)
              }
            )
          },
          error => {
            console.log(error.status)
          }
        )
        console.log(this.username)
      }
    }

    else{
      this.user = this.userService.currentUserValue
      this.link += this.user['LINKEDIN']
      this.friendsProfile = false
      console.log(this.user)
      this.photoProfile = '../assets/' + this.user.USERNAME + '.jpeg'
    }
   }

  ngOnInit() {
  }

  backFriends(){
    this.router.navigate(['/home/friends/'])
  }

  async confirmLogout(){
    this.logoutCheck = true
    const alert = await this.alertController.create({
      header: 'Confirm Logout',
      message: 'Are you sure you want to logout?',
      buttons: [{
        text: 'Yes',
        handler: () =>{
          this.logout()
        }
      },{ text: 'No'}]
    })

    await alert.present()
  }

  logout(){
    console.log('called logout')
    this.userService.logout()
    this.username = ""
    window.location.reload()
  }

  processFriendStatus(status){
    if (status == null){
      this.showAddFriend = true
    }
    else if(status.STATUS == 1){
      console.log('friends!')
      this.showFriendship = true;

    }
    else if (status.STATUS == 0 && this.friendStatus.RECEIVE_NAME == this.currentUser.USERNAME){
      this.acceptRequest = true
      console.log('accept their request ')
    }
    else if (status.STATUS == 0 && this.friendStatus.SEND_NAME == this.currentUser.USERNAME){
      this.pendingRequest = true
      this.pendingText = "Pending Request"
      console.log('request already sent')
    }
  }

  postFriendship(){
    console.log('adding friend')
    this.apiService.post_friendship(this.currentUser.USERNAME, this.user.USERNAME).subscribe(
      data => {
        console.log(data)
        this.friendButtonText = 'request sent'
      },
      error => {
        console.log(error)
        this.friendButtonText = 'request sent'
      }
    )
  }

  accept(){
    this.acceptText = "Request Accepted!"
    console.log(this.username)
    console.log(this.currentUser.USERNAME)
    this.apiService.respond(1,this.username,this.currentUser.USERNAME).subscribe(
      data => {
        console.log('worked')
      },
      error => {
        console.log(error)
      }

    )
  }


}
