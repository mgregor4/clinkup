import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { MessagesPage } from '../messages/messages.page';

const routes: Routes = [
  {
    path: 'home',
    component: TabsPage,
    children: [
      {
        path: 'clinkups',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../clinkups/clinkups.module').then(m => m.ClinkupsPageModule)
          }
        ]
      },
      {
        path: 'food-drink',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../food-drink/food-drink.module').then(m => m.FoodDrinkPageModule)
          }
        ]
      },
      {
        path: 'friends',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../friends/friends.module').then(m => m.FriendsPageModule)
          },
          {
            path: 'profile/:username',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../profile/profile.module').then(m => m.ProfilePageModule)
              }
            ]
          }
        ]
      },
      {
        path: 'messages',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../messages/messages.module').then(m => m.MessagesPageModule)
          },
          {
            path: 'conversation/:username',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../conversation/conversation.module').then(m => m.ConversationPageModule)
              }
            ]
          },
          {
            path: 'profile/:username',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../profile/profile.module').then(m => m.ProfilePageModule)
              }
            ]
          }
        ]
      },
      {
        path: 'travel',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../travel/travel.module').then(m => m.TravelPageModule)
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../profile/profile.module').then(m => m.ProfilePageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: 'home/clinkups',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'home/clinkups',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
