import { Component } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../Classes/user';
import { Router } from '@angular/router';
import { prepareEventListenerParameters } from '@angular/compiler/src/render3/view/template';
import { WebSocketService } from '../services/web-socket.service';
import { ToastController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  // user: User;
  public listen_sub: Subscription;

  constructor(private userService: UserService, public router: Router, private toastController: ToastController, private websocket: WebSocketService, private apiService: ApiService) {
    // this.userService.currentUser.subscribe(
    //   user => {
    //     console.log()
    //     user ? this.user = user : this.router.navigate(['auth']);
    //   }
    // );
  }

  ionViewWillEnter() {
    // this.getAllConverations();
    this.listen_sub = this.websocket.listen(`message-${this.userService.currentUserValue.USERNAME}`).subscribe(
      data => {
        console.log(data);
        console.log(this.userService.currentUserValue.USERNAME)
        if (data['receive_name'] == this.userService.currentUserValue.USERNAME) {
          this.apiService.get_single_user_username(data['send_name']).subscribe(
            full_user => {
              this.newMessage(full_user, data['text']);
            },
            error => {
              console.log(error);
            }
          )
        }
      }
    )
  }

  // getAllConverations() {
  //   this.apiService.get_user_conversations(this.userService.currentUserValue.USERNAME).subscribe(
  //     data => {
  //       this.conversations = data;
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   )
  // }

  async newMessage(user: User, text: string) {
    const toast = await this.toastController.create({
      header: user.NAME,
      message: text,
      position: 'top',
      duration: 4000,
      buttons: [
         {
           text: 'Respond',
           handler: () => {
             this.router.navigate(['/home/messages/conversation',user.USERNAME]);
           }
        }
      ]
    });
    toast.present();
  }


}
