import { Router } from '@angular/router';
import { throwError as observableThrowError, BehaviorSubject, Observable } from 'rxjs';
import { User } from '../Classes/user';
import { Travel } from '../Classes/travel';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })

export class TravelService {


  constructor(public httpClient: HttpClient, public router: Router) {}

  public getNextTrip(allTrips : Travel[]) : Travel {
    let currentDate = new Date()

    let nextTrip = new Travel()
    for (let trip of allTrips){
      let tripDate = new Date(trip['START_DATE'])

      if (tripDate > currentDate){
        nextTrip = trip

        break
      }
    }

    return nextTrip
  }

  convertDate(dateStr: string){ // date in format YYYY-MM-DD returns date as MM/DD/YY
    let dateArr = dateStr.split('-')
    return dateArr[1] + '/' + dateArr[2] + '/' + dateArr[0].charAt(2) + dateArr[0].charAt(3)
  }

  public getCurrentTrip(allTrips : Travel[]) : Travel {
    let currentDate = new Date()
    //console.log(currentDate)
    let currentTrip = new Travel()
    for (let trip of allTrips){
      let tripStart = new Date(trip['START_DATE'])
      let tripEnd = new Date(trip['END_DATE'])
      //console.log(tripDate)
      if (tripStart < currentDate && tripEnd > currentDate){
        currentTrip = trip
        //console.log('here')
        break
      }
    }

    return currentTrip
  }

  public checkOverlap(allTrips : Travel[], sdate, edate) : number {
    for (let t of allTrips){
      console.log(t)
      if ((t['START_DATE'] > sdate && t['START_DATE'] < edate) || (t['START_DATE'] < sdate && t['END_DATE'] > edate)|| (t['END_DATE'] > sdate && t['END_DATE'] < edate) ){
         console.log(t)
         return 0
       }
    }

  return 1
  }





}
