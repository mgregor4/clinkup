import { Router } from '@angular/router';
import { throwError as observableThrowError, BehaviorSubject, Observable } from 'rxjs';
import { User } from '../Classes/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })

export class UserService {

  public currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(public httpClient: HttpClient, public router: Router) {
    var previousUser;

    try {
      previousUser = JSON.parse(localStorage.getItem('currentUser'));
      localStorage.setItem('loggedIn', "1");
      if (previousUser == null) {
        throw "User Not logged in!";
      }
    } catch {
      previousUser = null;
      localStorage.setItem('loggedIn', "0");
    }

    this.currentUserSubject = new BehaviorSubject<User>(previousUser);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
      return this.currentUserSubject.value;
  }

  login(user) {
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.currentUserSubject.next(user);
    this.router.navigate(['']);
    localStorage.setItem('loggedIn', "1");
  }

  updateUser(user) {
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.currentUserSubject.next(user);
  }

  logout() {
      localStorage.removeItem('currentUser');
      this.currentUserSubject.next(null);
      this.router.navigate(['auth']);
      localStorage.setItem('loggedIn', "0");
  }

}
