import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { User } from '../Classes/user';
import { Travel } from '../Classes/travel';
import { Friend } from '../Classes/friend';
import { Clinkup } from '../Classes/clinkup';
import { Venue } from '../Classes/venue';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  PHP_API_SERVER = "http://34.206.226.160:8000";

  constructor(private httpClient: HttpClient) { }

  get_single_user(username: string, password: string){

    return this.httpClient.get<User>(`${this.PHP_API_SERVER}/get_single_user.php?username=${username}&password=${password}`).pipe(catchError(this.errorHandler));
  }

  post_user(user: User){
    return this.httpClient.post<any>(`${this.PHP_API_SERVER}/post_user_revised.php`, user).pipe(catchError(this.errorHandler));
  }

  post_travel(travel : Travel){
    console.log('called apiservice correctly')
    return this.httpClient.post<any>(`${this.PHP_API_SERVER}/post_travel.php`, travel).pipe(catchError(this.errorHandler));
  }

  get_travel(user: string){
    return this.httpClient.get<any>(`${this.PHP_API_SERVER}/get_travel.php?username=${user}`).pipe(catchError(this.errorHandler))
  }

  get_users_search(user: string, name: string){
    return this.httpClient.get<any>(`${this.PHP_API_SERVER}/get_users_search.php?user=${user}&name=${name}`).pipe(catchError(this.errorHandler));
  }

  post_friendship(send_name: string, receive_name: string) {
    return this.httpClient.post<any>(`${this.PHP_API_SERVER}/post_friendship.php`,{"SEND_NAME": send_name, "RECEIVE_NAME": receive_name}).pipe(catchError(this.errorHandler));
  }

  get_friend_requests(username) {
    return this.httpClient.get<any>(`${this.PHP_API_SERVER}/get_friend_requests.php?username=${username}`).pipe(catchError(this.errorHandler));
  }

  get_friends(username) {
    return this.httpClient.get<any>(`${this.PHP_API_SERVER}/get_friends.php?user=${username}`).pipe(catchError(this.errorHandler));
  }

  respond(status: number, send: string, receive: string){
    return this.httpClient.post<any>(`${this.PHP_API_SERVER}/put_friend_response.php`, {"STATUS": status, "SEND_NAME": send, "RECEIVE_NAME": receive}).pipe(catchError(this.errorHandler));
  }

  get_single_user_username(username: string){
    return this.httpClient.get<User>(`${this.PHP_API_SERVER}/get_user_by_username.php?username=${username}`).pipe(catchError(this.errorHandler));
  }

  get_conversation(send_name: string, receive_name: string) {
    var d = new Date();
    return this.httpClient.get<any>(`${this.PHP_API_SERVER}/get_conversation.php?send_name=${send_name}&receive_name=${receive_name}&offset=${d.getTimezoneOffset()/60}`).pipe(catchError(this.errorHandler));
  }

  get_user_conversations(username: string) {
    var d = new Date();
    return this.httpClient.get<any>(`${this.PHP_API_SERVER}/get_user_conversations.php?user=${username}&offset=${d.getTimezoneOffset()/60}`).pipe(catchError(this.errorHandler));
  }

  put_message_read(send: string, receive: string) {
    return this.httpClient.post<any>(`${this.PHP_API_SERVER}/put_message_read.php`, {"SEND_NAME": send, "RECEIVE_NAME": receive}).pipe(catchError(this.errorHandler));
  }

  get_friend_status(SEND_NAME: string, RECEIVE_NAME: string){
    return this.httpClient.get<Friend>(`${this.PHP_API_SERVER}/get_friend_status.php?SEND_NAME=${SEND_NAME}&RECEIVE_NAME=${RECEIVE_NAME}`)
  }

  get_travel_from_city(username : string, city : string){
    return this.httpClient.get<any>(`${this.PHP_API_SERVER}/get_travel_from_city.php?username=${username}&city=${city}`).pipe(catchError(this.errorHandler))
  }

  get_clinkup(username: string){
    return this.httpClient.get<Clinkup>(`${this.PHP_API_SERVER}/get_clinkup.php?SEND_NAME=${username}`).pipe(catchError(this.errorHandler))
  }

  post_clinkup(clinkup: Clinkup) {
    return this.httpClient.post<any>(`${this.PHP_API_SERVER}/post_clinkup.php`, clinkup).pipe(catchError(this.errorHandler));
  }

  post_venue(venue: Venue) {
    return this.httpClient.post<any>(`${this.PHP_API_SERVER}/post_venue.php`, venue).pipe(catchError(this.errorHandler));
  }

  put_clinkup(clinkup: Clinkup) {
    return this.httpClient.post<any>(`${this.PHP_API_SERVER}/put_clinkup.php`, clinkup).pipe(catchError(this.errorHandler));
  }

  get_yelp(term: string, location: string){
    return this.httpClient.get<any>(`${this.PHP_API_SERVER}/get_yelp.php?term=${term}&location=${location}`).pipe(catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error);
  }

}
