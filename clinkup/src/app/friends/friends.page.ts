import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Observable, BehaviorSubject } from 'rxjs';
import { ApiService } from '../services/api.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import {
   debounceTime, distinctUntilChanged, switchMap, first
 } from 'rxjs/operators';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.page.html',
  styleUrls: ['./friends.page.scss'],
})
export class FriendsPage implements OnInit {

  current_user = {};
  friendRequests = [];
  friends = [];
  searchResults1$: Observable<any[]> = new Observable<any[]>();
  // searchResults$: Observable<any[]> = new Observable<any[]>();
  searchTermsUsers = new BehaviorSubject<string[]>(['','']);
  profilePholo: string = '../assets/profile.png'


  constructor(private apiService: ApiService, private userService: UserService, private router: Router, public toastController: ToastController) {

  }

  ionViewWillEnter() {
    this.getFriendRequests();
    this.getFriends();
  }

  ngOnInit() {
  }

  searchResults$ = this.searchTermsUsers.pipe(
    // wait 300ms after each keystroke before considering the term
    debounceTime(300),
    // ignore new term if same as previous term
    distinctUntilChanged(),
    // switch to new search observable each time the term changes
    switchMap((term: string[]) => this.apiService.get_users_search(this.userService.currentUserValue.USERNAME,term[0]))

  )

  search_users(name) {
    this.searchTermsUsers.next([name]);
    // this.searchResults1$ = this.searchResults$.pipe(first(), debounceTime(300))
  }

  getFriendRequests() {
    this.apiService.get_friend_requests(this.userService.currentUserValue.USERNAME).subscribe(
      data => {
        this.friendRequests = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  getFriends() {
    this.apiService.get_friends(this.userService.currentUserValue.USERNAME).subscribe(
      data => {
        this.friends = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  addFriend(receive_name: string) {
    this.apiService.post_friendship(this.userService.currentUserValue.USERNAME,receive_name).subscribe(
      data => {
      },
      error => {
        console.log(error);
      }
    );
  }

  async refreshRequests(event) {
    await this.getFriendRequests();
    await this.getFriends();
    event.target.complete();
  }

  viewProfile(username: string) {
    this.router.navigate(['home/friends/profile',username]);
  }

  respond(response: number, send: string, receive: string, send_fullname: string) {
    this.apiService.respond(response, send, receive).subscribe(
      data => {
        this.getFriendRequests();
        if (response > 0) {
          this.acceptToast(send_fullname);
        } else {
          this.declineToast(send_fullname);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  async acceptToast(name: string) {
    const toast = await this.toastController.create({
      message: `Succesfully added friend, ${name}!`,
      color: 'primary',
      duration: 1000
    });
    toast.present();
  }

  async declineToast(name: string) {
    const toast = await this.toastController.create({
      message: `Declined friend request from ${name}!`,
      color: 'tertiary',
      duration: 1000
    });
    toast.present();
  }

  photoProfile(username: string){
    return '../assets/' + username + '.jpeg'
  }

}
