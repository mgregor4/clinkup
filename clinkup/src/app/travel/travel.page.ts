import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Travel } from '../Classes/travel';
import { User } from '../Classes/user';
import { UserService } from '../services/user.service';
import { TravelService } from '../services/travel.service';

@Component({
  selector: 'app-travel',
  templateUrl: './travel.page.html',
  styleUrls: ['./travel.page.scss'],
})
export class TravelPage implements OnInit {
  startDate: string
  endDate: string
  errorStringTravel: string
  travel : Travel
  home : boolean
  log : boolean
  view : boolean
  person : string
  travelRequest : any
  showTravel : boolean
  user: User
  noTravel : boolean
  nextTrip : any
  isUpcomingTrip : boolean
  showViewButton : boolean
  canAdd : boolean



  constructor(private apiService: ApiService, private userService: UserService, private travelService : TravelService) {
    this.startDate=""
    this.endDate=""
    this.errorStringTravel=""
    this.travel = new Travel()
    this.home = true
    this.log = false
    this.view = false
    this.person = ""
    this.travelRequest = ""
    this.showTravel = false
    this.noTravel = false
    this.nextTrip = ""
    this.isUpcomingTrip = false
    this.showViewButton = false
    this.canAdd = false

    this.userService.currentUser.subscribe(
      user => {
        this.user = user;
      }
    );

    this.callGetTravel()
  }


  ngOnInit() {
  }

  callGetTravel(){
    this.apiService.get_travel(this.userService.currentUserValue.USERNAME).subscribe(
      data => {
        this.nextTrip = data

        // get all travel past, present, future
        if (this.nextTrip){
          let data = this.travelService.getNextTrip(this.nextTrip)
            if (data['USERNAME']){
              this.nextTrip = data
              this.isUpcomingTrip = true
            }

          this.showViewButton = true
        }
      },
      error => {
        this.canAdd = true
        console.log(error.status)
      }
    )
  }


  submitTravel(sdate: string, edate: string, city: string, state: string, hotel: string){
    if (this.canAdd){
      if (!sdate || !edate || !city || !state || !hotel){
        this.errorStringTravel = 'all fields required'
      }

      else {
        this.travel['USERNAME'] = this.user['USERNAME']
        this.travel['START_DATE'] = sdate
        this.travel['END_DATE'] = edate
        this.travel['CITY'] = city
        this.travel['STATE'] = state
        this.travel['HOTEL'] = hotel

        this.apiService.post_travel(this.travel).subscribe(
          data => {
            this.apiService.get_travel(this.user['USERNAME']).subscribe(
              data => {
                this.nextTrip = data

                // get all travel past, present, future
                if (this.nextTrip){
                  let data = this.travelService.getNextTrip(this.nextTrip)
                    if (data['USERNAME']){
                      this.nextTrip = data
                      this.isUpcomingTrip = true
                    }


                  this.showViewButton = true
                }
              },
              error => {
                this.canAdd = true
                console.log(error.status)
              }
            )
          },
          error => {
            console.log(error.status)
          }
        )
        this.home = true
        this.log = false
      }

    this.canAdd = false
    }

    else {

      this.apiService.get_travel(this.user['USERNAME']).subscribe(
        data => {

          let check = this.travelService.checkOverlap(data, sdate, edate)

          if (check == 0){
            this.errorStringTravel = 'Already Traveling During These Dates'
          }

          if (!sdate || !edate || !city || !state || !hotel){
            this.errorStringTravel = 'all fields required'
            check = 0
          }

          if (check != 0){
            this.travel['USERNAME'] = this.user['USERNAME']
            this.travel['START_DATE'] = sdate
            this.travel['END_DATE'] = edate
            this.travel['CITY'] = city
            this.travel['STATE'] = state
            this.travel['HOTEL'] = hotel

            this.apiService.post_travel(this.travel).subscribe(
              data => {
                this.apiService.get_travel(this.user['USERNAME']).subscribe(
                  data => {
                    this.nextTrip = data

                    // get all travel past, present, future
                    if (this.nextTrip){
                      let data = this.travelService.getNextTrip(this.nextTrip)
                        if (data['USERNAME']){
                          this.nextTrip = data
                          this.isUpcomingTrip = true
                        }
                      this.showViewButton = true
                    }
                  },
                  error => {
                    this.canAdd = true
                    console.log(error.status)


                  }

                )
              },
              error => {
                console.log(error.status)
              }
            )
            this.home = true
            this.log = false

          }
        }

      ,
      error =>{
        console.log(error)
      }
    )
  }

  }

  logTravel(){
    this.log = true
    this.home = false
  }

  viewTravel(){
    this.view = true
    this.home = false
  }

  returnHome(){
    this.home = true
    this.view = false
    this.log = false
    this.startDate = ""
    this.endDate = ""
  }

  getTravel(){
    this.view = true
    this.home = false
    this.apiService.get_travel(this.userService.currentUserValue.USERNAME).subscribe(
      data => {
        this.travelRequest = data
        if (this.travelRequest){
          this.showTravel = true
        }
        else{
          this.noTravel = true
        }
      },
      error => {
        console.log(error.status)
      }

    )

  }


  async refreshRequests(event) {
    await this.callGetTravel()
    event.target.complete();
  }


}
