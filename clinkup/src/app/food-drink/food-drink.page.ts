import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { UserService } from '../services/user.service';
import { cities, states } from '../../data/cities';

@Component({
  selector: 'app-food-drink',
  templateUrl: './food-drink.page.html',
  styleUrls: ['./food-drink.page.scss'],
})
export class FoodDrinkPage implements OnInit {

  public recommendations = [];
  public search_results = [];
  public states = states;
  public cities = cities;

  constructor(private apiService: ApiService, private userService: UserService) {
  }

  // ionViewDidEnter() {
  //   this.get_yelp_rec();
  // }

  get_yelp_rec() {
    var search_term = `${this.userService.currentUserValue.FOODPREF} ${this.userService.currentUserValue.DRINKPREF} ${this.userService.currentUserValue.DIETARY != 'none' ? ' '+this.userService.currentUserValue.DIETARY : ''}`;
    var location = `${this.userService.currentUserValue.CITY}, ${this.userService.currentUserValue.STATE}`
    console.log(search_term)
    this.apiService.get_yelp(search_term, location).subscribe(
      data => {
        if (data) {
          this.recommendations = [];
          for (let r of data['businesses']) {
            r['categories'] = r['categories'].map( x => x['title']);
            this.recommendations.push(r);
          }
        }
      },
      error => {
        console.log(error)
      }
    )
  }

  search_yelp(search_term: string, city: string, state: string) {
    var location = `${city}${state != 'D.C.' ? ', '+state : ''}`
    console.log(search_term)
    this.apiService.get_yelp(search_term, location).subscribe(
      data => {
        if (data) {
          this.search_results = [];
          for (let r of data['businesses']) {
            r['categories'] = r['categories'].map( x => x['title']);
            this.search_results.push(r);
          }
        }
      },
      error => {
        console.log(error)
      }
    )
  }

  city_search(term: string) {
    return this.cities.filter(c => c['city'].slice(0,term.length).toLowerCase() == term.toLowerCase()).slice(0,10);
  }

  select_city(selection,city_input,state_input) {
    city_input['value'] = selection['city'];
    state_input['value'] = selection['state'];
  }

  city_change(state_input) {
    state_input.value = ''
  }

  ngOnInit() {
    this.get_yelp_rec();
    
  }

}
