export class  User {
    USERNAME: string = null;
    PASSWORD: string = null;
    NAME: string = null;
    CITY: string = null;
    STATE: string = null;
    LINKEDIN: string = null;
    COMPANY: string = null;
    PRIVACY: number = 0;
    PHOTO: string = null;
    UNIVERSITY: string = null;
    BIOGRAPHY: string = null;
    FOODPREF: string = null;
    DRINKPREF: string = null;
    DIETARY: string = null;
}
