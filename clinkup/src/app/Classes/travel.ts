export class  Travel {
    USERNAME: string = null;
    START_DATE: string = null;
    END_DATE: string = null;
    CITY: string = null;
    STATE: string = null;
    HOTEL: string = null;
}
