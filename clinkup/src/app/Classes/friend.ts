export class  Friend {
    SEND_NAME: string = null;
    RECEIVE_NAME: string = null;
    START_DATE: string = null;
    STATUS: number = null;
}