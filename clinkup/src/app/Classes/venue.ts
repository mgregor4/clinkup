export class  Venue {
    VENUEID: string = null;
    NAME: string = null;
    PHOTO_URL: string = null;
    ADDRESS: string = null;
}