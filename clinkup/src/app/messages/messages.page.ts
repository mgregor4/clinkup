import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { WebSocketService } from '../services/web-socket.service';
import { ApiService } from '../services/api.service';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {

  public conversations = [];
  public friends = [];
  public search_mode = false;
  public listen_sub: Subscription;
  @Input()
  set input(conversations) {
    this.conversations = conversations || [];
  }
  // @Input()
  // set inp(input) {
  //   console.log(input);
  //   if (input.length) {
  //     this.conversations = input;
  //   }
  // }
  @ViewChild('search', {static: false}) searchBar: any;

  constructor(private websocket: WebSocketService, private apiService: ApiService, private userService: UserService, private router: Router, private route: ActivatedRoute) {
    if (this.router.url.includes('conversation')) {
      this.search_mode = false;
      this.getFriends();
      // this.getAllConverations();
      this.listen_sub = this.websocket.listen(`message-${this.userService.currentUserValue.USERNAME}`).subscribe(
        data => {
          console.log(data);
          this.getAllConverations();
        }
      )
    }
  }

  ionViewWillEnter() {
    this.search_mode = false;
    this.getFriends();
    this.getAllConverations();
    this.listen_sub = this.websocket.listen(`message-${this.userService.currentUserValue.USERNAME}`).subscribe(
      data => {
        console.log(data);
        this.getAllConverations();
      }
    )
  }

  getAllConverations() {
    this.apiService.get_user_conversations(this.userService.currentUserValue.USERNAME).subscribe(
      data => {
        this.conversations = data;
      },
      error => {
        console.log(error);
      }
    )
  }

  loadConvo(username: string) {
    this.router.navigate(['home/messages/conversation',username]);
  }


  getFriends() {
    this.apiService.get_friends(this.userService.currentUserValue.USERNAME).subscribe(
      data => {
        this.friends = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.listen_sub.unsubscribe();
  }

  toggleSearchMode() {
    this.search_mode = !this.search_mode;
    if (this.search_mode) {
      console.log(this.searchBar);
      setTimeout(() => this.searchBar.el.setFocus());
    }
  }

  ionViewWillLeave() {
    this.listen_sub.unsubscribe();
  }

  photoProfile(username: any){
    return '../assets/' + username + '.jpeg'
  }
}
