import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MessagesComponentModule } from './messages-component.module';
import { MessagesPage } from './messages.page';

@NgModule({
  imports: [
    MessagesComponentModule,
    RouterModule.forChild([{ path: '', component: MessagesPage }])
  ]
})
export class MessagesPageModule {}
