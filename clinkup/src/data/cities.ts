export const states = [
 'AL','AK','AZ','AR','CA','CO','CT','DE','D.C.','FL','GA',
 'HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA',
 'MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND',
 'OH','OK','OR','PA','RI','SC','SD','TN','TX','UT',
 'VT','VA','WA','WV','WI','WY'
];

export const cities = [
    {
        "city": "New York",
        "state": "NY"
    },
    {
        "city": "Los Angeles",
        "state": "CA"
    },
    {
        "city": "Chicago",
        "state": "IL"
    },
    {
        "city": "Houston",
        "state": "TX"
    },
    {
        "city": "Philadelphia",
        "state": "PA"
    },
    {
        "city": "Phoenix",
        "state": "AZ"
    },
    {
        "city": "San Antonio",
        "state": "TX"
    },
    {
        "city": "San Diego",
        "state": "CA"
    },
    {
        "city": "Dallas",
        "state": "TX"
    },
    {
        "city": "San Jose",
        "state": "CA"
    },
    {
        "city": "Austin",
        "state": "TX"
    },
    {
        "city": "Indianapolis",
        "state": "IN"
    },
    {
        "city": "Jacksonville",
        "state": "FL"
    },
    {
        "city": "San Francisco",
        "state": "CA"
    },
    {
        "city": "Columbus",
        "state": "OH"
    },
    {
        "city": "Charlotte",
        "state": "NC"
    },
    {
        "city": "Fort Worth",
        "state": "TX"
    },
    {
        "city": "Detroit",
        "state": "MI"
    },
    {
        "city": "El Paso",
        "state": "TX"
    },
    {
        "city": "Memphis",
        "state": "TN"
    },
    {
        "city": "Seattle",
        "state": "WA"
    },
    {
        "city": "Denver",
        "state": "CO"
    },
    {
        "city": "Washington D.C.",
        "state": "D.C."
    },
    {
        "city": "Boston",
        "state": "MA"
    },
    {
        "city": "Nashville",
        "state": "TN"
    },
    {
        "city": "Baltimore",
        "state": "MD"
    },
    {
        "city": "Oklahoma City",
        "state": "OK"
    },
    {
        "city": "Louisville",
        "state": "KY"
    },
    {
        "city": "Portland",
        "state": "OR"
    },
    {
        "city": "Las Vegas",
        "state": "NV"
    },
    {
        "city": "Milwaukee",
        "state": "WI"
    },
    {
        "city": "Albuquerque",
        "state": "NM"
    },
    {
        "city": "Tucson",
        "state": "AZ"
    },
    {
        "city": "Fresno",
        "state": "CA"
    },
    {
        "city": "Sacramento",
        "state": "CA"
    },
    {
        "city": "Long Beach",
        "state": "CA"
    },
    {
        "city": "Kansas City",
        "state": "MO"
    },
    {
        "city": "Mesa",
        "state": "AZ"
    },
    {
        "city": "Virginia Beach",
        "state": "VA"
    },
    {
        "city": "Atlanta",
        "state": "GA"
    },
    {
        "city": "Colorado Springs",
        "state": "CO"
    },
    {
        "city": "Omaha",
        "state": "NE"
    },
    {
        "city": "Raleigh",
        "state": "NC"
    },
    {
        "city": "Miami",
        "state": "FL"
    },
    {
        "city": "Oakland",
        "state": "CA"
    },
    {
        "city": "Minneapolis",
        "state": "MN"
    },
    {
        "city": "Tulsa",
        "state": "OK"
    },
    {
        "city": "Cleveland",
        "state": "OH"
    },
    {
        "city": "Wichita",
        "state": "KS"
    },
    {
        "city": "Arlington",
        "state": "TX"
    },
    {
        "city": "New Orleans",
        "state": "LA"
    },
    {
        "city": "Bakersfield",
        "state": "CA"
    },
    {
        "city": "Tampa",
        "state": "FL"
    },
    {
        "city": "Honolulu",
        "state": "HI"
    },
    {
        "city": "Aurora",
        "state": "CO"
    },
    {
        "city": "Anaheim",
        "state": "CA"
    },
    {
        "city": "Santa Ana",
        "state": "CA"
    },
    {
        "city": "St. Louis",
        "state": "MO"
    },
    {
        "city": "Riverside",
        "state": "CA"
    },
    {
        "city": "Corpus Christi",
        "state": "TX"
    },
    {
        "city": "Lexington-Fayette",
        "state": "KY"
    },
    {
        "city": "Pittsburgh",
        "state": "PA"
    },
    {
        "city": "Anchorage",
        "state": "AL"
    },
    {
        "city": "Stockton",
        "state": "CA"
    },
    {
        "city": "Cincinnati",
        "state": "OH"
    },
    {
        "city": "St. Paul",
        "state": "MN"
    },
    {
        "city": "Toledo",
        "state": "OH"
    },
    {
        "city": "Greensboro",
        "state": "NC"
    },
    {
        "city": "Newark",
        "state": "NJ"
    },
    {
        "city": "Plano",
        "state": "TX"
    },
    {
        "city": "Henderson",
        "state": "NV"
    },
    {
        "city": "Lincoln",
        "state": "NE"
    },
    {
        "city": "Buffalo",
        "state": "NY"
    },
    {
        "city": "Jersey City",
        "state": "NJ"
    },
    {
        "city": "Chula Vista",
        "state": "CA"
    },
    {
        "city": "Fort Wayne",
        "state": "IN"
    },
    {
        "city": "Orlando",
        "state": "FL"
    },
    {
        "city": "St. Petersburg",
        "state": "FL"
    },
    {
        "city": "Chandler",
        "state": "AZ"
    },
    {
        "city": "Laredo",
        "state": "TX"
    },
    {
        "city": "Norfolk",
        "state": "VA"
    },
    {
        "city": "Durham",
        "state": "NC"
    },
    {
        "city": "Madison",
        "state": "WI"
    },
    {
        "city": "Lubbock",
        "state": "TX"
    },
    {
        "city": "Irvine",
        "state": "CA"
    },
    {
        "city": "Winston-Salem",
        "state": "NC"
    },
    {
        "city": "Glendale",
        "state": "AZ"
    },
    {
        "city": "Garland",
        "state": "TX"
    },
    {
        "city": "Hialeah",
        "state": "FL"
    },
    {
        "city": "Reno",
        "state": "NV"
    },
    {
        "city": "Chesapeake",
        "state": "VA"
    },
    {
        "city": "Gilbert",
        "state": "AZ"
    },
    {
        "city": "Baton Rouge",
        "state": "LA"
    },
    {
        "city": "Irving",
        "state": "TX"
    },
    {
        "city": "Scottsdale",
        "state": "AZ"
    },
    {
        "city": "North Las Vegas",
        "state": "NV"
    },
    {
        "city": "Fremont",
        "state": "CA"
    },
    {
        "city": "Boise City",
        "state": "ID"
    },
    {
        "city": "Richmond",
        "state": "VA"
    },
    {
        "city": "San Bernardino",
        "state": "CA"
    },
    {
        "city": "Birmingham",
        "state": "AL"
    },
    {
        "city": "Spokane",
        "state": "WA"
    },
    {
        "city": "Rochester",
        "state": "NY"
    },
    {
        "city": "Des Moines",
        "state": "IA"
    },
    {
        "city": "Modesto",
        "state": "CA"
    },
    {
        "city": "Fayetteville",
        "state": "NC"
    },
    {
        "city": "Tacoma",
        "state": "WA"
    },
    {
        "city": "Oxnard",
        "state": "CA"
    },
    {
        "city": "Fontana",
        "state": "CA"
    },
    {
        "city": "Columbus",
        "state": "GA"
    },
    {
        "city": "Montgomery",
        "state": "AL"
    },
    {
        "city": "Moreno Valley",
        "state": "CA"
    },
    {
        "city": "Shreveport",
        "state": "LA"
    },
    {
        "city": "Aurora",
        "state": "IL"
    },
    {
        "city": "Yonkers",
        "state": "NY"
    },
    {
        "city": "Akron",
        "state": "OH"
    },
    {
        "city": "Huntington Beach",
        "state": "CA"
    },
    {
        "city": "Little Rock",
        "state": "AR"
    },
    {
        "city": "Augusta-Richmond County",
        "state": "GA"
    },
    {
        "city": "Amarillo",
        "state": "TX"
    },
    {
        "city": "Glendale",
        "state": "CA"
    },
    {
        "city": "Mobile",
        "state": "AL"
    },
    {
        "city": "Grand Rapids",
        "state": "MI"
    },
    {
        "city": "Salt Lake City",
        "state": "UT"
    },
    {
        "city": "Tallahassee",
        "state": "FL"
    },
    {
        "city": "Huntsville",
        "state": "AL"
    },
    {
        "city": "Grand Prairie",
        "state": "TX"
    },
    {
        "city": "Knoxville",
        "state": "TN"
    },
    {
        "city": "Worcester",
        "state": "MA"
    },
    {
        "city": "Newport News",
        "state": "VA"
    },
    {
        "city": "Brownsville",
        "state": "TX"
    },
    {
        "city": "Overland Park",
        "state": "KS"
    },
    {
        "city": "Santa Clarita",
        "state": "CA"
    },
    {
        "city": "Providence",
        "state": "RI"
    },
    {
        "city": "Garden Grove",
        "state": "CA"
    },
    {
        "city": "Chattanooga",
        "state": "TN"
    },
    {
        "city": "Oceanside",
        "state": "CA"
    },
    {
        "city": "Jackson",
        "state": "MS"
    },
    {
        "city": "Fort Lauderdale",
        "state": "FL"
    },
    {
        "city": "Santa Rosa",
        "state": "CA"
    },
    {
        "city": "Rancho Cucamonga",
        "state": "CA"
    },
    {
        "city": "Port St. Lucie",
        "state": "FL"
    },
    {
        "city": "Tempe",
        "state": "AZ"
    },
    {
        "city": "Ontario",
        "state": "CA"
    },
    {
        "city": "Vancouver",
        "state": "WA"
    },
    {
        "city": "Cape Coral",
        "state": "FL"
    },
    {
        "city": "Sioux Falls",
        "state": "South Dakota"
    },
    {
        "city": "Springfield",
        "state": "MO"
    },
    {
        "city": "Peoria",
        "state": "AZ"
    },
    {
        "city": "Pembroke Pines",
        "state": "FL"
    },
    {
        "city": "Elk Grove",
        "state": "CA"
    },
    {
        "city": "Salem",
        "state": "OR"
    },
    {
        "city": "Lancaster",
        "state": "CA"
    },
    {
        "city": "Corona",
        "state": "CA"
    },
    {
        "city": "Eugene",
        "state": "OR"
    },
    {
        "city": "Palmdale",
        "state": "CA"
    },
    {
        "city": "Salinas",
        "state": "CA"
    },
    {
        "city": "Springfield",
        "state": "MA"
    },
    {
        "city": "Pasadena",
        "state": "TX"
    },
    {
        "city": "Fort Collins",
        "state": "CO"
    },
    {
        "city": "Hayward",
        "state": "CA"
    },
    {
        "city": "Pomona",
        "state": "CA"
    },
    {
        "city": "Cary",
        "state": "NC"
    },
    {
        "city": "Rockford",
        "state": "IL"
    },
    {
        "city": "Alexandria",
        "state": "VA"
    },
    {
        "city": "Escondido",
        "state": "CA"
    },
    {
        "city": "McKinney",
        "state": "TX"
    },
    {
        "city": "Kansas City",
        "state": "KS"
    },
    {
        "city": "Joliet",
        "state": "IL"
    },
    {
        "city": "Sunnyvale",
        "state": "CA"
    },
    {
        "city": "Torrance",
        "state": "CA"
    },
    {
        "city": "Bridgeport",
        "state": "CT"
    },
    {
        "city": "Lakewood",
        "state": "CO"
    },
    {
        "city": "Hollywood",
        "state": "FL"
    },
    {
        "city": "Paterson",
        "state": "NJ"
    },
    {
        "city": "Naperville",
        "state": "IL"
    },
    {
        "city": "Syracuse",
        "state": "NY"
    },
    {
        "city": "Mesquite",
        "state": "TX"
    },
    {
        "city": "Dayton",
        "state": "OH"
    },
    {
        "city": "Savannah",
        "state": "GA"
    },
    {
        "city": "Clarksville",
        "state": "TN"
    },
    {
        "city": "Orange",
        "state": "CA"
    },
    {
        "city": "Pasadena",
        "state": "CA"
    },
    {
        "city": "Fullerton",
        "state": "CA"
    },
    {
        "city": "Killeen",
        "state": "TX"
    },
    {
        "city": "Frisco",
        "state": "TX"
    },
    {
        "city": "Hampton",
        "state": "VA"
    },
    {
        "city": "McAllen",
        "state": "TX"
    },
    {
        "city": "Warren",
        "state": "MI"
    },
    {
        "city": "Bellevue",
        "state": "WA"
    },
    {
        "city": "West Valley City",
        "state": "UT"
    },
    {
        "city": "Columbia",
        "state": "SC"
    },
    {
        "city": "Olathe",
        "state": "KS"
    },
    {
        "city": "Sterling Heights",
        "state": "MI"
    },
    {
        "city": "New Haven",
        "state": "CT"
    },
    {
        "city": "Miramar",
        "state": "FL"
    },
    {
        "city": "Waco",
        "state": "TX"
    },
    {
        "city": "Thousand Oaks",
        "state": "CA"
    },
    {
        "city": "Cedar Rapids",
        "state": "IA"
    },
    {
        "city": "Charleston",
        "state": "SC"
    },
    {
        "city": "Visalia",
        "state": "CA"
    },
    {
        "city": "Topeka",
        "state": "KS"
    },
    {
        "city": "Elizabeth",
        "state": "NJ"
    },
    {
        "city": "Gainesville",
        "state": "FL"
    },
    {
        "city": "Thornton",
        "state": "CO"
    },
    {
        "city": "Roseville",
        "state": "CA"
    },
    {
        "city": "Carrollton",
        "state": "TX"
    },
    {
        "city": "Coral Springs",
        "state": "FL"
    },
    {
        "city": "Stamford",
        "state": "CT"
    },
    {
        "city": "Simi Valley",
        "state": "CA"
    },
    {
        "city": "Concord",
        "state": "CA"
    },
    {
        "city": "Hartford",
        "state": "CT"
    },
    {
        "city": "Kent",
        "state": "WA"
    },
    {
        "city": "Lafayette",
        "state": "LA"
    },
    {
        "city": "Midland",
        "state": "TX"
    },
    {
        "city": "Surprise",
        "state": "AZ"
    },
    {
        "city": "Denton",
        "state": "TX"
    },
    {
        "city": "Victorville",
        "state": "CA"
    },
    {
        "city": "Evansville",
        "state": "IN"
    },
    {
        "city": "Santa Clara",
        "state": "CA"
    },
    {
        "city": "Abilene",
        "state": "TX"
    },
    {
        "city": "Athens-Clarke County",
        "state": "GA"
    },
    {
        "city": "Vallejo",
        "state": "CA"
    },
    {
        "city": "Allentown",
        "state": "PA"
    },
    {
        "city": "Norman",
        "state": "OK"
    },
    {
        "city": "Beaumont",
        "state": "TX"
    },
    {
        "city": "Independence",
        "state": "MO"
    },
    {
        "city": "Murfreesboro",
        "state": "TN"
    },
    {
        "city": "Ann Arbor",
        "state": "MI"
    },
    {
        "city": "Springfield",
        "state": "IL"
    },
    {
        "city": "Berkeley",
        "state": "CA"
    },
    {
        "city": "Peoria",
        "state": "IL"
    },
    {
        "city": "Provo",
        "state": "UT"
    },
    {
        "city": "El Monte",
        "state": "CA"
    },
    {
        "city": "Columbia",
        "state": "MO"
    },
    {
        "city": "Lansing",
        "state": "MI"
    },
    {
        "city": "Fargo",
        "state": "ND"
    },
    {
        "city": "Downey",
        "state": "CA"
    },
    {
        "city": "Costa Mesa",
        "state": "CA"
    },
    {
        "city": "Wilmington",
        "state": "NC"
    },
    {
        "city": "Arvada",
        "state": "CO"
    },
    {
        "city": "Inglewood",
        "state": "CA"
    },
    {
        "city": "Miami Gardens",
        "state": "FL"
    },
    {
        "city": "Carlsbad",
        "state": "CA"
    },
    {
        "city": "Westminster",
        "state": "CO"
    },
    {
        "city": "Rochester",
        "state": "MN"
    },
    {
        "city": "Odessa",
        "state": "TX"
    },
    {
        "city": "Manchester",
        "state": "NH"
    },
    {
        "city": "Elgin",
        "state": "IL"
    },
    {
        "city": "West Jordan",
        "state": "UT"
    },
    {
        "city": "Round Rock",
        "state": "TX"
    },
    {
        "city": "Clearwater",
        "state": "FL"
    },
    {
        "city": "Waterbury",
        "state": "CT"
    },
    {
        "city": "Gresham",
        "state": "OR"
    },
    {
        "city": "Fairfield",
        "state": "CA"
    },
    {
        "city": "Billings",
        "state": "MT"
    },
    {
        "city": "Lowell",
        "state": "MA"
    },
    {
        "city": "San Buenaventura (Ventura)",
        "state": "CA"
    },
    {
        "city": "Pueblo",
        "state": "CO"
    },
    {
        "city": "High Point",
        "state": "NC"
    },
    {
        "city": "West Covina",
        "state": "CA"
    },
    {
        "city": "Richmond",
        "state": "CA"
    },
    {
        "city": "Murrieta",
        "state": "CA"
    },
    {
        "city": "Cambridge",
        "state": "MA"
    },
    {
        "city": "Antioch",
        "state": "CA"
    },
    {
        "city": "Temecula",
        "state": "CA"
    },
    {
        "city": "Norwalk",
        "state": "CA"
    },
    {
        "city": "Centennial",
        "state": "CO"
    },
    {
        "city": "Everett",
        "state": "WA"
    },
    {
        "city": "Palm Bay",
        "state": "FL"
    },
    {
        "city": "Wichita Falls",
        "state": "TX"
    },
    {
        "city": "Green Bay",
        "state": "WI"
    },
    {
        "city": "Daly City",
        "state": "CA"
    },
    {
        "city": "Burbank",
        "state": "CA"
    },
    {
        "city": "Richardson",
        "state": "TX"
    },
    {
        "city": "Pompano Beach",
        "state": "FL"
    },
    {
        "city": "North Charleston",
        "state": "SC"
    },
    {
        "city": "Broken Arrow",
        "state": "OK"
    },
    {
        "city": "Boulder",
        "state": "CO"
    },
    {
        "city": "West Palm Beach",
        "state": "FL"
    },
    {
        "city": "Santa Maria",
        "state": "CA"
    },
    {
        "city": "El Cajon",
        "state": "CA"
    },
    {
        "city": "Davenport",
        "state": "IA"
    },
    {
        "city": "Rialto",
        "state": "CA"
    },
    {
        "city": "Las Cruces",
        "state": "NM"
    },
    {
        "city": "San Mateo",
        "state": "CA"
    },
    {
        "city": "Lewisville",
        "state": "TX"
    },
    {
        "city": "South Bend",
        "state": "IN"
    },
    {
        "city": "Lakeland",
        "state": "FL"
    },
    {
        "city": "Erie",
        "state": "PA"
    },
    {
        "city": "Tyler",
        "state": "TX"
    },
    {
        "city": "Pearland",
        "state": "TX"
    },
    {
        "city": "College Station",
        "state": "TX"
    },
    {
        "city": "Kenosha",
        "state": "WI"
    },
    {
        "city": "Sandy Springs",
        "state": "GA"
    },
    {
        "city": "Clovis",
        "state": "CA"
    },
    {
        "city": "Flint",
        "state": "MI"
    },
    {
        "city": "Roanoke",
        "state": "VA"
    },
    {
        "city": "Albany",
        "state": "NY"
    },
    {
        "city": "Jurupa Valley",
        "state": "CA"
    },
    {
        "city": "Compton",
        "state": "CA"
    },
    {
        "city": "San Angelo",
        "state": "TX"
    },
    {
        "city": "Hillsboro",
        "state": "OR"
    },
    {
        "city": "Lawton",
        "state": "OK"
    },
    {
        "city": "Renton",
        "state": "WA"
    },
    {
        "city": "Vista",
        "state": "CA"
    },
    {
        "city": "Davie",
        "state": "FL"
    },
    {
        "city": "Greeley",
        "state": "CO"
    },
    {
        "city": "Mission Viejo",
        "state": "CA"
    },
    {
        "city": "Portsmouth",
        "state": "VA"
    },
    {
        "city": "Dearborn",
        "state": "MI"
    },
    {
        "city": "South Gate",
        "state": "CA"
    },
    {
        "city": "Tuscaloosa",
        "state": "AL"
    },
    {
        "city": "Livonia",
        "state": "MI"
    },
    {
        "city": "New Bedford",
        "state": "MA"
    },
    {
        "city": "Vacaville",
        "state": "CA"
    },
    {
        "city": "Brockton",
        "state": "MA"
    },
    {
        "city": "Roswell",
        "state": "GA"
    },
    {
        "city": "Beaverton",
        "state": "OR"
    },
    {
        "city": "Quincy",
        "state": "MA"
    },
    {
        "city": "Sparks",
        "state": "NV"
    },
    {
        "city": "Yakima",
        "state": "WA"
    },
    {
        "city": "Lee's Summit",
        "state": "MO"
    },
    {
        "city": "Federal Way",
        "state": "WA"
    },
    {
        "city": "Carson",
        "state": "CA"
    },
    {
        "city": "Santa Monica",
        "state": "CA"
    },
    {
        "city": "Hesperia",
        "state": "CA"
    },
    {
        "city": "Allen",
        "state": "TX"
    },
    {
        "city": "Rio Rancho",
        "state": "NM"
    },
    {
        "city": "Yuma",
        "state": "AZ"
    },
    {
        "city": "Westminster",
        "state": "CA"
    },
    {
        "city": "Orem",
        "state": "UT"
    },
    {
        "city": "Lynn",
        "state": "MA"
    },
    {
        "city": "Redding",
        "state": "CA"
    },
    {
        "city": "Spokane Valley",
        "state": "WA"
    },
    {
        "city": "Miami Beach",
        "state": "FL"
    },
    {
        "city": "League City",
        "state": "TX"
    },
    {
        "city": "Lawrence",
        "state": "KS"
    },
    {
        "city": "Santa Barbara",
        "state": "CA"
    },
    {
        "city": "Plantation",
        "state": "FL"
    },
    {
        "city": "Sandy",
        "state": "UT"
    },
    {
        "city": "Sunrise",
        "state": "FL"
    },
    {
        "city": "Macon",
        "state": "GA"
    },
    {
        "city": "Longmont",
        "state": "CO"
    },
    {
        "city": "Boca Raton",
        "state": "FL"
    },
    {
        "city": "San Marcos",
        "state": "CA"
    },
    {
        "city": "Greenville",
        "state": "NC"
    },
    {
        "city": "Waukegan",
        "state": "IL"
    },
    {
        "city": "Fall River",
        "state": "MA"
    },
    {
        "city": "Chico",
        "state": "CA"
    },
    {
        "city": "Newton",
        "state": "MA"
    },
    {
        "city": "San Leandro",
        "state": "CA"
    },
    {
        "city": "Reading",
        "state": "PA"
    },
    {
        "city": "Norwalk",
        "state": "CT"
    },
    {
        "city": "Fort Smith",
        "state": "AR"
    },
    {
        "city": "Newport Beach",
        "state": "CA"
    },
    {
        "city": "Asheville",
        "state": "NC"
    },
    {
        "city": "Nashua",
        "state": "NH"
    },
    {
        "city": "Edmond",
        "state": "OK"
    },
    {
        "city": "Whittier",
        "state": "CA"
    },
    {
        "city": "Nampa",
        "state": "ID"
    },
    {
        "city": "Bloomington",
        "state": "MN"
    },
    {
        "city": "Deltona",
        "state": "FL"
    },
    {
        "city": "Hawthorne",
        "state": "CA"
    },
    {
        "city": "Duluth",
        "state": "MN"
    },
    {
        "city": "Carmel",
        "state": "IN"
    },
    {
        "city": "Suffolk",
        "state": "VA"
    },
    {
        "city": "Clifton",
        "state": "NJ"
    },
    {
        "city": "Citrus Heights",
        "state": "CA"
    },
    {
        "city": "Livermore",
        "state": "CA"
    },
    {
        "city": "Tracy",
        "state": "CA"
    },
    {
        "city": "Alhambra",
        "state": "CA"
    },
    {
        "city": "Kirkland",
        "state": "WA"
    },
    {
        "city": "Trenton",
        "state": "NJ"
    },
    {
        "city": "Ogden",
        "state": "UT"
    },
    {
        "city": "Hoover",
        "state": "AL"
    },
    {
        "city": "Cicero",
        "state": "IL"
    },
    {
        "city": "Fishers",
        "state": "IN"
    },
    {
        "city": "Sugar Land",
        "state": "TX"
    },
    {
        "city": "Danbury",
        "state": "CT"
    },
    {
        "city": "Meridian",
        "state": "ID"
    },
    {
        "city": "Indio",
        "state": "CA"
    },
    {
        "city": "Concord",
        "state": "NC"
    },
    {
        "city": "Menifee",
        "state": "CA"
    },
    {
        "city": "Champaign",
        "state": "IL"
    },
    {
        "city": "Buena Park",
        "state": "CA"
    },
    {
        "city": "Troy",
        "state": "MI"
    },
    {
        "city": "O'Fallon",
        "state": "MO"
    },
    {
        "city": "Johns Creek",
        "state": "GA"
    },
    {
        "city": "Bellingham",
        "state": "WA"
    },
    {
        "city": "Westland",
        "state": "MI"
    },
    {
        "city": "Bloomington",
        "state": "IN"
    },
    {
        "city": "Sioux City",
        "state": "IA"
    },
    {
        "city": "Warwick",
        "state": "RI"
    },
    {
        "city": "Hemet",
        "state": "CA"
    },
    {
        "city": "Longview",
        "state": "TX"
    },
    {
        "city": "Farmington Hills",
        "state": "MI"
    },
    {
        "city": "Bend",
        "state": "OR"
    },
    {
        "city": "Lakewood",
        "state": "CA"
    },
    {
        "city": "Merced",
        "state": "CA"
    },
    {
        "city": "Mission",
        "state": "TX"
    },
    {
        "city": "Chino",
        "state": "CA"
    },
    {
        "city": "Redwood City",
        "state": "CA"
    },
    {
        "city": "Edinburg",
        "state": "TX"
    },
    {
        "city": "Cranston",
        "state": "RI"
    },
    {
        "city": "Parma",
        "state": "OH"
    },
    {
        "city": "New Rochelle",
        "state": "NY"
    },
    {
        "city": "Lake Forest",
        "state": "CA"
    },
    {
        "city": "Napa",
        "state": "CA"
    },
    {
        "city": "Hammond",
        "state": "IN"
    },
    {
        "city": "Fayetteville",
        "state": "AR"
    },
    {
        "city": "Bloomington",
        "state": "IL"
    },
    {
        "city": "Avondale",
        "state": "AZ"
    },
    {
        "city": "Somerville",
        "state": "MA"
    },
    {
        "city": "Palm Coast",
        "state": "FL"
    },
    {
        "city": "Bryan",
        "state": "TX"
    },
    {
        "city": "Gary",
        "state": "IN"
    },
    {
        "city": "Largo",
        "state": "FL"
    },
    {
        "city": "Brooklyn Park",
        "state": "MN"
    },
    {
        "city": "Tustin",
        "state": "CA"
    },
    {
        "city": "Racine",
        "state": "WI"
    },
    {
        "city": "Deerfield Beach",
        "state": "FL"
    },
    {
        "city": "Lynchburg",
        "state": "VA"
    },
    {
        "city": "Mountain View",
        "state": "CA"
    },
    {
        "city": "Medford",
        "state": "OR"
    },
    {
        "city": "Lawrence",
        "state": "MA"
    },
    {
        "city": "Bellflower",
        "state": "CA"
    },
    {
        "city": "Melbourne",
        "state": "FL"
    },
    {
        "city": "St. Joseph",
        "state": "MO"
    },
    {
        "city": "Camden",
        "state": "NJ"
    },
    {
        "city": "St. George",
        "state": "UT"
    },
    {
        "city": "Kennewick",
        "state": "WA"
    },
    {
        "city": "Baldwin Park",
        "state": "CA"
    },
    {
        "city": "Chino Hills",
        "state": "CA"
    },
    {
        "city": "Alameda",
        "state": "CA"
    },
    {
        "city": "Albany",
        "state": "GA"
    },
    {
        "city": "Arlington Heights",
        "state": "IL"
    },
    {
        "city": "Scranton",
        "state": "PA"
    },
    {
        "city": "Evanston",
        "state": "IL"
    },
    {
        "city": "Kalamazoo",
        "state": "MI"
    },
    {
        "city": "Baytown",
        "state": "TX"
    },
    {
        "city": "Upland",
        "state": "CA"
    },
    {
        "city": "Springdale",
        "state": "AR"
    },
    {
        "city": "Bethlehem",
        "state": "PA"
    },
    {
        "city": "Schaumburg",
        "state": "IL"
    },
    {
        "city": "Mount Pleasant",
        "state": "SC"
    },
    {
        "city": "Auburn",
        "state": "WA"
    },
    {
        "city": "Decatur",
        "state": "IL"
    },
    {
        "city": "San Ramon",
        "state": "CA"
    },
    {
        "city": "Pleasanton",
        "state": "CA"
    },
    {
        "city": "Wyoming",
        "state": "MI"
    },
    {
        "city": "Lake Charles",
        "state": "LA"
    },
    {
        "city": "Plymouth",
        "state": "MN"
    },
    {
        "city": "Bolingbrook",
        "state": "IL"
    },
    {
        "city": "Pharr",
        "state": "TX"
    },
    {
        "city": "Appleton",
        "state": "WI"
    },
    {
        "city": "Gastonia",
        "state": "NC"
    },
    {
        "city": "Folsom",
        "state": "CA"
    },
    {
        "city": "Southfield",
        "state": "MI"
    },
    {
        "city": "Rochester Hills",
        "state": "MI"
    },
    {
        "city": "New Britain",
        "state": "CT"
    },
    {
        "city": "Goodyear",
        "state": "AZ"
    },
    {
        "city": "Canton",
        "state": "OH"
    },
    {
        "city": "Warner Robins",
        "state": "GA"
    },
    {
        "city": "Union City",
        "state": "CA"
    },
    {
        "city": "Perris",
        "state": "CA"
    },
    {
        "city": "Manteca",
        "state": "CA"
    },
    {
        "city": "Iowa City",
        "state": "IA"
    },
    {
        "city": "Jonesboro",
        "state": "AR"
    },
    {
        "city": "Wilmington",
        "state": "DE"
    },
    {
        "city": "Lynwood",
        "state": "CA"
    },
    {
        "city": "Loveland",
        "state": "CO"
    },
    {
        "city": "Pawtucket",
        "state": "RI"
    },
    {
        "city": "Boynton Beach",
        "state": "FL"
    },
    {
        "city": "Waukesha",
        "state": "WI"
    },
    {
        "city": "Gulfport",
        "state": "MS"
    },
    {
        "city": "Apple Valley",
        "state": "CA"
    },
    {
        "city": "Passaic",
        "state": "NJ"
    },
    {
        "city": "Rapid City",
        "state": "South Dakota"
    },
    {
        "city": "Layton",
        "state": "UT"
    },
    {
        "city": "Lafayette",
        "state": "IN"
    },
    {
        "city": "Turlock",
        "state": "CA"
    },
    {
        "city": "Muncie",
        "state": "IN"
    },
    {
        "city": "Temple",
        "state": "TX"
    },
    {
        "city": "Missouri City",
        "state": "TX"
    },
    {
        "city": "Redlands",
        "state": "CA"
    },
    {
        "city": "Santa Fe",
        "state": "NM"
    },
    {
        "city": "Lauderhill",
        "state": "FL"
    },
    {
        "city": "Milpitas",
        "state": "CA"
    },
    {
        "city": "Palatine",
        "state": "IL"
    },
    {
        "city": "Missoula",
        "state": "MT"
    },
    {
        "city": "Rock Hill",
        "state": "SC"
    },
    {
        "city": "Jacksonville",
        "state": "NC"
    },
    {
        "city": "Franklin",
        "state": "TN"
    },
    {
        "city": "Flagstaff",
        "state": "AZ"
    },
    {
        "city": "Flower Mound",
        "state": "TX"
    },
    {
        "city": "Weston",
        "state": "FL"
    },
    {
        "city": "Waterloo",
        "state": "IA"
    },
    {
        "city": "Union City",
        "state": "NJ"
    },
    {
        "city": "Mount Vernon",
        "state": "NY"
    },
    {
        "city": "Fort Myers",
        "state": "FL"
    },
    {
        "city": "Dothan",
        "state": "AL"
    },
    {
        "city": "Rancho Cordova",
        "state": "CA"
    },
    {
        "city": "Redondo Beach",
        "state": "CA"
    },
    {
        "city": "Jackson",
        "state": "TN"
    },
    {
        "city": "Pasco",
        "state": "WA"
    },
    {
        "city": "St. Charles",
        "state": "MO"
    },
    {
        "city": "Eau Claire",
        "state": "WI"
    },
    {
        "city": "North Richland Hills",
        "state": "TX"
    },
    {
        "city": "Bismarck",
        "state": "ND"
    },
    {
        "city": "Yorba Linda",
        "state": "CA"
    },
    {
        "city": "Kenner",
        "state": "LA"
    },
    {
        "city": "Walnut Creek",
        "state": "CA"
    },
    {
        "city": "Frederick",
        "state": "MD"
    },
    {
        "city": "Oshkosh",
        "state": "WI"
    },
    {
        "city": "Pittsburg",
        "state": "CA"
    },
    {
        "city": "Palo Alto",
        "state": "CA"
    },
    {
        "city": "Bossier City",
        "state": "LA"
    },
    {
        "city": "Portland",
        "state": "ME"
    },
    {
        "city": "St. Cloud",
        "state": "MN"
    },
    {
        "city": "Davis",
        "state": "CA"
    },
    {
        "city": "South San Francisco",
        "state": "CA"
    },
    {
        "city": "Camarillo",
        "state": "CA"
    },
    {
        "city": "North Little Rock",
        "state": "AR"
    },
    {
        "city": "Schenectady",
        "state": "NY"
    },
    {
        "city": "Gaithersburg",
        "state": "MD"
    },
    {
        "city": "Harlingen",
        "state": "TX"
    },
    {
        "city": "Woodbury",
        "state": "MN"
    },
    {
        "city": "Eagan",
        "state": "MN"
    },
    {
        "city": "Yuba City",
        "state": "CA"
    },
    {
        "city": "Maple Grove",
        "state": "MN"
    },
    {
        "city": "Youngstown",
        "state": "OH"
    },
    {
        "city": "Skokie",
        "state": "IL"
    },
    {
        "city": "Kissimmee",
        "state": "FL"
    },
    {
        "city": "Johnson City",
        "state": "TN"
    },
    {
        "city": "Victoria",
        "state": "TX"
    },
    {
        "city": "San Clemente",
        "state": "CA"
    },
    {
        "city": "Bayonne",
        "state": "NJ"
    },
    {
        "city": "Laguna Niguel",
        "state": "CA"
    },
    {
        "city": "East Orange",
        "state": "NJ"
    },
    {
        "city": "Shawnee",
        "state": "KS"
    },
    {
        "city": "Homestead",
        "state": "FL"
    },
    {
        "city": "Rockville",
        "state": "MD"
    },
    {
        "city": "Delray Beach",
        "state": "FL"
    },
    {
        "city": "Janesville",
        "state": "WI"
    },
    {
        "city": "Conway",
        "state": "AR"
    },
    {
        "city": "Pico Rivera",
        "state": "CA"
    },
    {
        "city": "Lorain",
        "state": "OH"
    },
    {
        "city": "Montebello",
        "state": "CA"
    },
    {
        "city": "Lodi",
        "state": "CA"
    },
    {
        "city": "New Braunfels",
        "state": "TX"
    },
    {
        "city": "Marysville",
        "state": "WA"
    },
    {
        "city": "Tamarac",
        "state": "FL"
    },
    {
        "city": "Madera",
        "state": "CA"
    },
    {
        "city": "Conroe",
        "state": "TX"
    },
    {
        "city": "Santa Cruz",
        "state": "CA"
    },
    {
        "city": "Eden Prairie",
        "state": "MN"
    },
    {
        "city": "Cheyenne",
        "state": "WY"
    },
    {
        "city": "Daytona Beach",
        "state": "FL"
    },
    {
        "city": "Alpharetta",
        "state": "GA"
    },
    {
        "city": "Hamilton",
        "state": "OH"
    },
    {
        "city": "Waltham",
        "state": "MA"
    },
    {
        "city": "Coon Rapids",
        "state": "MN"
    },
    {
        "city": "Haverhill",
        "state": "MA"
    },
    {
        "city": "Council Bluffs",
        "state": "IA"
    },
    {
        "city": "Taylor",
        "state": "MI"
    },
    {
        "city": "Utica",
        "state": "NY"
    },
    {
        "city": "Ames",
        "state": "IA"
    },
    {
        "city": "La Habra",
        "state": "CA"
    },
    {
        "city": "Encinitas",
        "state": "CA"
    },
    {
        "city": "Bowling Green",
        "state": "KY"
    },
    {
        "city": "Burnsville",
        "state": "MN"
    },
    {
        "city": "Greenville",
        "state": "SC"
    },
    {
        "city": "West Des Moines",
        "state": "IA"
    },
    {
        "city": "Cedar Park",
        "state": "TX"
    },
    {
        "city": "Tulare",
        "state": "CA"
    },
    {
        "city": "Monterey Park",
        "state": "CA"
    },
    {
        "city": "Vineland",
        "state": "NJ"
    },
    {
        "city": "Terre Haute",
        "state": "IN"
    },
    {
        "city": "North Miami",
        "state": "FL"
    },
    {
        "city": "Mansfield",
        "state": "TX"
    },
    {
        "city": "West Allis",
        "state": "WI"
    },
    {
        "city": "Bristol",
        "state": "CT"
    },
    {
        "city": "Taylorsville",
        "state": "UT"
    },
    {
        "city": "Malden",
        "state": "MA"
    },
    {
        "city": "Meriden",
        "state": "CT"
    },
    {
        "city": "Blaine",
        "state": "MN"
    },
    {
        "city": "Wellington",
        "state": "FL"
    },
    {
        "city": "Cupertino",
        "state": "CA"
    },
    {
        "city": "Springfield",
        "state": "OR"
    },
    {
        "city": "Rogers",
        "state": "AR"
    },
    {
        "city": "St. Clair Shores",
        "state": "MI"
    },
    {
        "city": "Gardena",
        "state": "CA"
    },
    {
        "city": "Pontiac",
        "state": "MI"
    },
    {
        "city": "National City",
        "state": "CA"
    },
    {
        "city": "Grand Junction",
        "state": "CO"
    },
    {
        "city": "Rocklin",
        "state": "CA"
    },
    {
        "city": "Chapel Hill",
        "state": "NC"
    },
    {
        "city": "Casper",
        "state": "WY"
    },
    {
        "city": "Broomfield",
        "state": "CO"
    },
    {
        "city": "Petaluma",
        "state": "CA"
    },
    {
        "city": "South Jordan",
        "state": "UT"
    },
    {
        "city": "Springfield",
        "state": "OH"
    },
    {
        "city": "Great Falls",
        "state": "MT"
    },
    {
        "city": "Lancaster",
        "state": "PA"
    },
    {
        "city": "North Port",
        "state": "FL"
    },
    {
        "city": "Lakewood",
        "state": "WA"
    },
    {
        "city": "Marietta",
        "state": "GA"
    },
    {
        "city": "San Rafael",
        "state": "CA"
    },
    {
        "city": "Royal Oak",
        "state": "MI"
    },
    {
        "city": "Des Plaines",
        "state": "IL"
    },
    {
        "city": "Huntington Park",
        "state": "CA"
    },
    {
        "city": "La Mesa",
        "state": "CA"
    },
    {
        "city": "Orland Park",
        "state": "IL"
    },
    {
        "city": "Auburn",
        "state": "AL"
    },
    {
        "city": "Lakeville",
        "state": "MN"
    },
    {
        "city": "Owensboro",
        "state": "KY"
    },
    {
        "city": "Moore",
        "state": "OK"
    },
    {
        "city": "Jupiter",
        "state": "FL"
    },
    {
        "city": "Idaho Falls",
        "state": "ID"
    },
    {
        "city": "Dubuque",
        "state": "IA"
    },
    {
        "city": "Bartlett",
        "state": "TN"
    },
    {
        "city": "Rowlett",
        "state": "TX"
    },
    {
        "city": "Novi",
        "state": "MI"
    },
    {
        "city": "White Plains",
        "state": "NY"
    },
    {
        "city": "Arcadia",
        "state": "CA"
    },
    {
        "city": "Redmond",
        "state": "WA"
    },
    {
        "city": "Lake Elsinore",
        "state": "CA"
    },
    {
        "city": "Ocala",
        "state": "FL"
    },
    {
        "city": "Tinley Park",
        "state": "IL"
    },
    {
        "city": "Port Orange",
        "state": "FL"
    },
    {
        "city": "Medford",
        "state": "MA"
    },
    {
        "city": "Oak Lawn",
        "state": "IL"
    },
    {
        "city": "Rocky Mount",
        "state": "NC"
    },
    {
        "city": "Kokomo",
        "state": "IN"
    },
    {
        "city": "Coconut Creek",
        "state": "FL"
    },
    {
        "city": "Bowie",
        "state": "MD"
    },
    {
        "city": "Berwyn",
        "state": "IL"
    },
    {
        "city": "Midwest City",
        "state": "OK"
    },
    {
        "city": "Fountain Valley",
        "state": "CA"
    },
    {
        "city": "Buckeye",
        "state": "AZ"
    },
    {
        "city": "Dearborn Heights",
        "state": "MI"
    },
    {
        "city": "Woodland",
        "state": "CA"
    },
    {
        "city": "Noblesville",
        "state": "IN"
    },
    {
        "city": "Valdosta",
        "state": "GA"
    },
    {
        "city": "Diamond Bar",
        "state": "CA"
    },
    {
        "city": "Manhattan",
        "state": "KS"
    },
    {
        "city": "Santee",
        "state": "CA"
    },
    {
        "city": "Taunton",
        "state": "MA"
    },
    {
        "city": "Sanford",
        "state": "FL"
    },
    {
        "city": "Kettering",
        "state": "OH"
    },
    {
        "city": "New Brunswick",
        "state": "NJ"
    },
    {
        "city": "Decatur",
        "state": "AL"
    },
    {
        "city": "Chicopee",
        "state": "MA"
    },
    {
        "city": "Anderson",
        "state": "IN"
    },
    {
        "city": "Margate",
        "state": "FL"
    },
    {
        "city": "Weymouth Town",
        "state": "MA"
    },
    {
        "city": "Hempstead",
        "state": "NY"
    },
    {
        "city": "Corvallis",
        "state": "OR"
    },
    {
        "city": "Eastvale",
        "state": "CA"
    },
    {
        "city": "Porterville",
        "state": "CA"
    },
    {
        "city": "West Haven",
        "state": "CT"
    },
    {
        "city": "Brentwood",
        "state": "CA"
    },
    {
        "city": "Paramount",
        "state": "CA"
    },
    {
        "city": "Grand Forks",
        "state": "ND"
    },
    {
        "city": "Georgetown",
        "state": "TX"
    },
    {
        "city": "St. Peters",
        "state": "MO"
    },
    {
        "city": "Shoreline",
        "state": "WA"
    },
    {
        "city": "Mount Prospect",
        "state": "IL"
    },
    {
        "city": "Hanford",
        "state": "CA"
    },
    {
        "city": "Normal",
        "state": "IL"
    },
    {
        "city": "Rosemead",
        "state": "CA"
    },
    {
        "city": "Lehi",
        "state": "UT"
    },
    {
        "city": "Pocatello",
        "state": "ID"
    },
    {
        "city": "Highland",
        "state": "CA"
    },
    {
        "city": "Novato",
        "state": "CA"
    },
    {
        "city": "Port Arthur",
        "state": "TX"
    },
    {
        "city": "Carson City",
        "state": "NV"
    },
    {
        "city": "San Marcos",
        "state": "TX"
    },
    {
        "city": "Hendersonville",
        "state": "TN"
    },
    {
        "city": "Elyria",
        "state": "OH"
    },
    {
        "city": "Revere",
        "state": "MA"
    },
    {
        "city": "Pflugerville",
        "state": "TX"
    },
    {
        "city": "Greenwood",
        "state": "IN"
    },
    {
        "city": "Bellevue",
        "state": "NE"
    },
    {
        "city": "Wheaton",
        "state": "IL"
    },
    {
        "city": "Smyrna",
        "state": "GA"
    },
    {
        "city": "Sarasota",
        "state": "FL"
    },
    {
        "city": "Blue Springs",
        "state": "MO"
    },
    {
        "city": "Colton",
        "state": "CA"
    },
    {
        "city": "Euless",
        "state": "TX"
    },
    {
        "city": "Castle Rock",
        "state": "CO"
    },
    {
        "city": "Cathedral City",
        "state": "CA"
    },
    {
        "city": "Kingsport",
        "state": "TN"
    },
    {
        "city": "Lake Havasu City",
        "state": "AZ"
    },
    {
        "city": "Pensacola",
        "state": "FL"
    },
    {
        "city": "Hoboken",
        "state": "NJ"
    },
    {
        "city": "Yucaipa",
        "state": "CA"
    },
    {
        "city": "Watsonville",
        "state": "CA"
    },
    {
        "city": "Richland",
        "state": "WA"
    },
    {
        "city": "Delano",
        "state": "CA"
    },
    {
        "city": "Hoffman Estates",
        "state": "IL"
    },
    {
        "city": "Florissant",
        "state": "MO"
    },
    {
        "city": "Placentia",
        "state": "CA"
    },
    {
        "city": "West New York",
        "state": "NJ"
    },
    {
        "city": "Dublin",
        "state": "CA"
    },
    {
        "city": "Oak Park",
        "state": "IL"
    },
    {
        "city": "Peabody",
        "state": "MA"
    },
    {
        "city": "Perth Amboy",
        "state": "NJ"
    },
    {
        "city": "Battle Creek",
        "state": "MI"
    },
    {
        "city": "Bradenton",
        "state": "FL"
    },
    {
        "city": "Gilroy",
        "state": "CA"
    },
    {
        "city": "Milford",
        "state": "CT"
    },
    {
        "city": "Albany",
        "state": "OR"
    },
    {
        "city": "Ankeny",
        "state": "IA"
    },
    {
        "city": "La Crosse",
        "state": "WI"
    },
    {
        "city": "Burlington",
        "state": "NC"
    },
    {
        "city": "DeSoto",
        "state": "TX"
    },
    {
        "city": "Harrisonburg",
        "state": "VA"
    },
    {
        "city": "Minnetonka",
        "state": "MN"
    },
    {
        "city": "Elkhart",
        "state": "IN"
    },
    {
        "city": "Lakewood",
        "state": "OH"
    },
    {
        "city": "Glendora",
        "state": "CA"
    },
    {
        "city": "Southaven",
        "state": "MS"
    },
    {
        "city": "Charleston",
        "state": "WV"
    },
    {
        "city": "Joplin",
        "state": "MO"
    },
    {
        "city": "Enid",
        "state": "OK"
    },
    {
        "city": "Palm Beach Gardens",
        "state": "FL"
    },
    {
        "city": "Brookhaven",
        "state": "GA"
    },
    {
        "city": "Plainfield",
        "state": "NJ"
    },
    {
        "city": "Grand Island",
        "state": "NE"
    },
    {
        "city": "Palm Desert",
        "state": "CA"
    },
    {
        "city": "Huntersville",
        "state": "NC"
    },
    {
        "city": "Tigard",
        "state": "OR"
    },
    {
        "city": "Lenexa",
        "state": "KS"
    },
    {
        "city": "Saginaw",
        "state": "MI"
    },
    {
        "city": "Kentwood",
        "state": "MI"
    },
    {
        "city": "Doral",
        "state": "FL"
    },
    {
        "city": "Apple Valley",
        "state": "MN"
    },
    {
        "city": "Grapevine",
        "state": "TX"
    },
    {
        "city": "Aliso Viejo",
        "state": "CA"
    },
    {
        "city": "Sammamish",
        "state": "WA"
    },
    {
        "city": "Casa Grande",
        "state": "AZ"
    },
    {
        "city": "Pinellas Park",
        "state": "FL"
    },
    {
        "city": "Troy",
        "state": "NY"
    },
    {
        "city": "West Sacramento",
        "state": "CA"
    },
    {
        "city": "Burien",
        "state": "WA"
    },
    {
        "city": "Commerce City",
        "state": "CO"
    },
    {
        "city": "Monroe",
        "state": "LA"
    },
    {
        "city": "Cerritos",
        "state": "CA"
    },
    {
        "city": "Downers Grove",
        "state": "IL"
    },
    {
        "city": "Coral Gables",
        "state": "FL"
    },
    {
        "city": "Wilson",
        "state": "NC"
    },
    {
        "city": "Niagara Falls",
        "state": "NY"
    },
    {
        "city": "Poway",
        "state": "CA"
    },
    {
        "city": "Edina",
        "state": "MN"
    },
    {
        "city": "Cuyahoga Falls",
        "state": "OH"
    },
    {
        "city": "Rancho Santa Margarita",
        "state": "CA"
    },
    {
        "city": "Harrisburg",
        "state": "PA"
    },
    {
        "city": "Huntington",
        "state": "WV"
    },
    {
        "city": "La Mirada",
        "state": "CA"
    },
    {
        "city": "Cypress",
        "state": "CA"
    },
    {
        "city": "Caldwell",
        "state": "ID"
    },
    {
        "city": "Logan",
        "state": "UT"
    },
    {
        "city": "Galveston",
        "state": "TX"
    },
    {
        "city": "Sheboygan",
        "state": "WI"
    },
    {
        "city": "Middletown",
        "state": "OH"
    },
    {
        "city": "Murray",
        "state": "UT"
    },
    {
        "city": "Roswell",
        "state": "NM"
    },
    {
        "city": "Parker",
        "state": "CO"
    },
    {
        "city": "Bedford",
        "state": "TX"
    },
    {
        "city": "East Lansing",
        "state": "MI"
    },
    {
        "city": "Methuen",
        "state": "MA"
    },
    {
        "city": "Covina",
        "state": "CA"
    },
    {
        "city": "Alexandria",
        "state": "LA"
    },
    {
        "city": "Olympia",
        "state": "WA"
    },
    {
        "city": "Euclid",
        "state": "OH"
    },
    {
        "city": "Mishawaka",
        "state": "IN"
    },
    {
        "city": "Salina",
        "state": "KS"
    },
    {
        "city": "Azusa",
        "state": "CA"
    },
    {
        "city": "Newark",
        "state": "OH"
    },
    {
        "city": "Chesterfield",
        "state": "MO"
    },
    {
        "city": "Leesburg",
        "state": "VA"
    },
    {
        "city": "Dunwoody",
        "state": "GA"
    },
    {
        "city": "Hattiesburg",
        "state": "MS"
    },
    {
        "city": "Roseville",
        "state": "MI"
    },
    {
        "city": "Bonita Springs",
        "state": "FL"
    },
    {
        "city": "Portage",
        "state": "MI"
    },
    {
        "city": "St. Louis Park",
        "state": "MN"
    },
    {
        "city": "Collierville",
        "state": "TN"
    },
    {
        "city": "Middletown",
        "state": "CT"
    },
    {
        "city": "Stillwater",
        "state": "OK"
    },
    {
        "city": "East Providence",
        "state": "RI"
    },
    {
        "city": "Lawrence",
        "state": "IN"
    },
    {
        "city": "Wauwatosa",
        "state": "WI"
    },
    {
        "city": "Mentor",
        "state": "OH"
    },
    {
        "city": "Ceres",
        "state": "CA"
    },
    {
        "city": "Cedar Hill",
        "state": "TX"
    },
    {
        "city": "Mansfield",
        "state": "OH"
    },
    {
        "city": "Binghamton",
        "state": "NY"
    },
    {
        "city": "Coeur d'Alene",
        "state": "ID"
    },
    {
        "city": "San Luis Obispo",
        "state": "CA"
    },
    {
        "city": "Minot",
        "state": "ND"
    },
    {
        "city": "Palm Springs",
        "state": "CA"
    },
    {
        "city": "Pine Bluff",
        "state": "AR"
    },
    {
        "city": "Texas City",
        "state": "TX"
    },
    {
        "city": "Summerville",
        "state": "SC"
    },
    {
        "city": "Twin Falls",
        "state": "ID"
    },
    {
        "city": "Jeffersonville",
        "state": "IN"
    },
    {
        "city": "San Jacinto",
        "state": "CA"
    },
    {
        "city": "Madison",
        "state": "AL"
    },
    {
        "city": "Altoona",
        "state": "PA"
    },
    {
        "city": "Columbus",
        "state": "IN"
    },
    {
        "city": "Beavercreek",
        "state": "OH"
    },
    {
        "city": "Apopka",
        "state": "FL"
    },
    {
        "city": "Elmhurst",
        "state": "IL"
    },
    {
        "city": "Maricopa",
        "state": "AZ"
    },
    {
        "city": "Farmington",
        "state": "NM"
    },
    {
        "city": "Glenview",
        "state": "IL"
    },
    {
        "city": "Cleveland Heights",
        "state": "OH"
    },
    {
        "city": "Draper",
        "state": "UT"
    },
    {
        "city": "Lincoln",
        "state": "CA"
    },
    {
        "city": "Sierra Vista",
        "state": "AZ"
    },
    {
        "city": "Lacey",
        "state": "WA"
    },
    {
        "city": "Biloxi",
        "state": "MS"
    },
    {
        "city": "Strongsville",
        "state": "OH"
    },
    {
        "city": "Barnstable Town",
        "state": "MA"
    },
    {
        "city": "Wylie",
        "state": "TX"
    },
    {
        "city": "Sayreville",
        "state": "NJ"
    },
    {
        "city": "Kannapolis",
        "state": "NC"
    },
    {
        "city": "Charlottesville",
        "state": "VA"
    },
    {
        "city": "Littleton",
        "state": "CO"
    },
    {
        "city": "Titusville",
        "state": "FL"
    },
    {
        "city": "Hackensack",
        "state": "NJ"
    },
    {
        "city": "Newark",
        "state": "CA"
    },
    {
        "city": "Pittsfield",
        "state": "MA"
    },
    {
        "city": "York",
        "state": "PA"
    },
    {
        "city": "Lombard",
        "state": "IL"
    },
    {
        "city": "Attleboro",
        "state": "MA"
    },
    {
        "city": "DeKalb",
        "state": "IL"
    },
    {
        "city": "Blacksburg",
        "state": "VA"
    },
    {
        "city": "Dublin",
        "state": "OH"
    },
    {
        "city": "Haltom City",
        "state": "TX"
    },
    {
        "city": "Lompoc",
        "state": "CA"
    },
    {
        "city": "El Centro",
        "state": "CA"
    },
    {
        "city": "Danville",
        "state": "CA"
    },
    {
        "city": "Jefferson City",
        "state": "MO"
    },
    {
        "city": "Cutler Bay",
        "state": "FL"
    },
    {
        "city": "Oakland Park",
        "state": "FL"
    },
    {
        "city": "North Miami Beach",
        "state": "FL"
    },
    {
        "city": "Freeport",
        "state": "NY"
    },
    {
        "city": "Moline",
        "state": "IL"
    },
    {
        "city": "Coachella",
        "state": "CA"
    },
    {
        "city": "Fort Pierce",
        "state": "FL"
    },
    {
        "city": "Smyrna",
        "state": "TN"
    },
    {
        "city": "Bountiful",
        "state": "UT"
    },
    {
        "city": "Fond du Lac",
        "state": "WI"
    },
    {
        "city": "Everett",
        "state": "MA"
    },
    {
        "city": "Danville",
        "state": "VA"
    },
    {
        "city": "Keller",
        "state": "TX"
    },
    {
        "city": "Belleville",
        "state": "IL"
    },
    {
        "city": "Bell Gardens",
        "state": "CA"
    },
    {
        "city": "Cleveland",
        "state": "TN"
    },
    {
        "city": "North Lauderdale",
        "state": "FL"
    },
    {
        "city": "Fairfield",
        "state": "OH"
    },
    {
        "city": "Salem",
        "state": "MA"
    },
    {
        "city": "Rancho Palos Verdes",
        "state": "CA"
    },
    {
        "city": "San Bruno",
        "state": "CA"
    },
    {
        "city": "Concord",
        "state": "NH"
    },
    {
        "city": "Burlington",
        "state": "VT"
    },
    {
        "city": "Apex",
        "state": "NC"
    },
    {
        "city": "Midland",
        "state": "MI"
    },
    {
        "city": "Altamonte Springs",
        "state": "FL"
    },
    {
        "city": "Hutchinson",
        "state": "KS"
    },
    {
        "city": "Buffalo Grove",
        "state": "IL"
    },
    {
        "city": "Urbandale",
        "state": "IA"
    },
    {
        "city": "State College",
        "state": "PA"
    },
    {
        "city": "Urbana",
        "state": "IL"
    },
    {
        "city": "Plainfield",
        "state": "IL"
    },
    {
        "city": "Manassas",
        "state": "VA"
    },
    {
        "city": "Bartlett",
        "state": "IL"
    },
    {
        "city": "Kearny",
        "state": "NJ"
    },
    {
        "city": "Oro Valley",
        "state": "AZ"
    },
    {
        "city": "Findlay",
        "state": "OH"
    },
    {
        "city": "Rohnert Park",
        "state": "CA"
    },
    {
        "city": "Westfield",
        "state": "MA"
    },
    {
        "city": "Linden",
        "state": "NJ"
    },
    {
        "city": "Sumter",
        "state": "SC"
    },
    {
        "city": "Wilkes-Barre",
        "state": "PA"
    },
    {
        "city": "Woonsocket",
        "state": "RI"
    },
    {
        "city": "Leominster",
        "state": "MA"
    },
    {
        "city": "Shelton",
        "state": "CT"
    },
    {
        "city": "Brea",
        "state": "CA"
    },
    {
        "city": "Covington",
        "state": "KY"
    },
    {
        "city": "Rockwall",
        "state": "TX"
    },
    {
        "city": "Meridian",
        "state": "MS"
    },
    {
        "city": "Riverton",
        "state": "UT"
    },
    {
        "city": "St. Cloud",
        "state": "FL"
    },
    {
        "city": "Quincy",
        "state": "IL"
    },
    {
        "city": "Morgan Hill",
        "state": "CA"
    },
    {
        "city": "Warren",
        "state": "OH"
    },
    {
        "city": "Edmonds",
        "state": "WA"
    },
    {
        "city": "Burleson",
        "state": "TX"
    },
    {
        "city": "Beverly",
        "state": "MA"
    },
    {
        "city": "Mankato",
        "state": "MN"
    },
    {
        "city": "Hagerstown",
        "state": "MD"
    },
    {
        "city": "Prescott",
        "state": "AZ"
    },
    {
        "city": "Campbell",
        "state": "CA"
    },
    {
        "city": "Cedar Falls",
        "state": "IA"
    },
    {
        "city": "Beaumont",
        "state": "CA"
    },
    {
        "city": "La Puente",
        "state": "CA"
    },
    {
        "city": "Crystal Lake",
        "state": "IL"
    },
    {
        "city": "Fitchburg",
        "state": "MA"
    },
    {
        "city": "Carol Stream",
        "state": "IL"
    },
    {
        "city": "Hickory",
        "state": "NC"
    },
    {
        "city": "Streamwood",
        "state": "IL"
    },
    {
        "city": "Norwich",
        "state": "CT"
    },
    {
        "city": "Coppell",
        "state": "TX"
    },
    {
        "city": "San Gabriel",
        "state": "CA"
    },
    {
        "city": "Holyoke",
        "state": "MA"
    },
    {
        "city": "Bentonville",
        "state": "AR"
    },
    {
        "city": "Florence",
        "state": "AL"
    },
    {
        "city": "Peachtree Corners",
        "state": "GA"
    },
    {
        "city": "Brentwood",
        "state": "TN"
    },
    {
        "city": "Bozeman",
        "state": "MT"
    },
    {
        "city": "New Berlin",
        "state": "WI"
    },
    {
        "city": "Goose Creek",
        "state": "SC"
    },
    {
        "city": "Huntsville",
        "state": "TX"
    },
    {
        "city": "Prescott Valley",
        "state": "AZ"
    },
    {
        "city": "Maplewood",
        "state": "MN"
    },
    {
        "city": "Romeoville",
        "state": "IL"
    },
    {
        "city": "Duncanville",
        "state": "TX"
    },
    {
        "city": "Atlantic City",
        "state": "NJ"
    },
    {
        "city": "Clovis",
        "state": "NM"
    },
    {
        "city": "The Colony",
        "state": "TX"
    },
    {
        "city": "Culver City",
        "state": "CA"
    },
    {
        "city": "Marlborough",
        "state": "MA"
    },
    {
        "city": "Hilton Head Island",
        "state": "SC"
    },
    {
        "city": "Moorhead",
        "state": "MN"
    },
    {
        "city": "Calexico",
        "state": "CA"
    },
    {
        "city": "Bullhead City",
        "state": "AZ"
    },
    {
        "city": "Germantown",
        "state": "TN"
    },
    {
        "city": "La Quinta",
        "state": "CA"
    },
    {
        "city": "Lancaster",
        "state": "OH"
    },
    {
        "city": "Wausau",
        "state": "WI"
    },
    {
        "city": "Sherman",
        "state": "TX"
    },
    {
        "city": "Ocoee",
        "state": "FL"
    },
    {
        "city": "Shakopee",
        "state": "MN"
    },
    {
        "city": "Woburn",
        "state": "MA"
    },
    {
        "city": "Bremerton",
        "state": "WA"
    },
    {
        "city": "Rock Island",
        "state": "IL"
    },
    {
        "city": "Muskogee",
        "state": "OK"
    },
    {
        "city": "Cape Girardeau",
        "state": "MO"
    },
    {
        "city": "Annapolis",
        "state": "MD"
    },
    {
        "city": "Greenacres",
        "state": "FL"
    },
    {
        "city": "Ormond Beach",
        "state": "FL"
    },
    {
        "city": "Hallandale Beach",
        "state": "FL"
    },
    {
        "city": "Stanton",
        "state": "CA"
    },
    {
        "city": "Puyallup",
        "state": "WA"
    },
    {
        "city": "Pacifica",
        "state": "CA"
    },
    {
        "city": "Hanover Park",
        "state": "IL"
    },
    {
        "city": "Hurst",
        "state": "TX"
    },
    {
        "city": "Lima",
        "state": "OH"
    },
    {
        "city": "Marana",
        "state": "AZ"
    },
    {
        "city": "Carpentersville",
        "state": "IL"
    },
    {
        "city": "Oakley",
        "state": "CA"
    },
    {
        "city": "Huber Heights",
        "state": "OH"
    },
    {
        "city": "Lancaster",
        "state": "TX"
    },
    {
        "city": "Montclair",
        "state": "CA"
    },
    {
        "city": "Wheeling",
        "state": "IL"
    },
    {
        "city": "Brookfield",
        "state": "WI"
    },
    {
        "city": "Park Ridge",
        "state": "IL"
    },
    {
        "city": "Florence",
        "state": "SC"
    },
    {
        "city": "Roy",
        "state": "UT"
    },
    {
        "city": "Winter Garden",
        "state": "FL"
    },
    {
        "city": "Chelsea",
        "state": "MA"
    },
    {
        "city": "Valley Stream",
        "state": "NY"
    },
    {
        "city": "Spartanburg",
        "state": "SC"
    },
    {
        "city": "Lake Oswego",
        "state": "OR"
    },
    {
        "city": "Friendswood",
        "state": "TX"
    },
    {
        "city": "Westerville",
        "state": "OH"
    },
    {
        "city": "Northglenn",
        "state": "CO"
    },
    {
        "city": "Phenix City",
        "state": "AL"
    },
    {
        "city": "Grove City",
        "state": "OH"
    },
    {
        "city": "Texarkana",
        "state": "TX"
    },
    {
        "city": "Addison",
        "state": "IL"
    },
    {
        "city": "Dover",
        "state": "DE"
    },
    {
        "city": "Lincoln Park",
        "state": "MI"
    },
    {
        "city": "Calumet City",
        "state": "IL"
    },
    {
        "city": "Muskegon",
        "state": "MI"
    },
    {
        "city": "Aventura",
        "state": "FL"
    },
    {
        "city": "Martinez",
        "state": "CA"
    },
    {
        "city": "Greenfield",
        "state": "WI"
    },
    {
        "city": "Apache Junction",
        "state": "AZ"
    },
    {
        "city": "Monrovia",
        "state": "CA"
    },
    {
        "city": "Weslaco",
        "state": "TX"
    },
    {
        "city": "Keizer",
        "state": "OR"
    },
    {
        "city": "Spanish Fork",
        "state": "UT"
    },
    {
        "city": "Beloit",
        "state": "WI"
    },
    {
        "city": "Panama City",
        "state": "FL"
    }
]
