<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';


  if (!isset($_GET['user'])) {
    $USER = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $USER = $_GET['user'];
  }

  if (!isset($_GET['offset'])) {
    $OFFSET = 0;
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $OFFSET = $_GET['offset'];
  }

  $messages = array();
  $query = "SELECT u.username, u.name, m.text, u.company, to_char(m.TIMESTAMP - :t_offset/24, 'fmHH:fmMI AM') AS M_TIME, m.read, m.send_name FROM users u, messages m,(SELECT username, max(timestamp) AS latest_time FROM (select send_name AS username, timestamp FROM messages WHERE receive_name = :username UNION SELECT receive_name AS username, timestamp FROM messages WHERE send_name = :username) s GROUP BY username) d WHERE u.username = d.username AND d.latest_time = m.timestamp ORDER BY m.timestamp DESC";
// SELECT u.username, u.name, m.text, u.company, to_char(m.TIMESTAMP - :t_offset/24, 'fmHH:MI AM') AS M_TIME, m.read FROM users u, messages m,(SELECT username, max(timestamp) AS latest_time FROM (select send_name AS username, timestamp FROM messages WHERE receive_name = :username UNION SELECT receive_name AS username, timestamp FROM messages WHERE send_name = :username) s GROUP BY username) d WHERE u.username = d.username AND d.latest_time = m.timestamp";

  $stmt = oci_parse($conn, $query);

  oci_bind_by_name($stmt, ":username", $USER);
  oci_bind_by_name($stmt, ":t_offset", $OFFSET);

  oci_execute($stmt);
  // $e = oci_error($stmt);
  // echo json_encode($e);
  $i = 0;
  while (($row = oci_fetch_array($stmt, OCI_ASSOC)) != false){
    $messages[$i] = $row;
    $i++;
   }

  oci_free_statement($stmt);

  // if (empty($userData) && $responseCode == 200){
  //
  //   echo $message;
  //   //echo json_encode($userData);
  //   $responseCode = 410;
  //   $responseString = 'HTTP/1.1 410 Bad Request no user found';
  // }
  // else {
  // }
  echo json_encode($messages);

  header($responseString, true, $responseCode);
  //header('HTTP/1.1 200 Good Request', true, 200);

  oci_close($conn);
?>
