<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';

  if (!isset($_GET['user'])) {
    $user = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $user = $_GET['user'];
  }


  $userData = array();
  $query = "SELECT u.USERNAME, u.NAME, u.COMPANY, u.CITY, u.STATE, f.* FROM users u, friends f WHERE f.STATUS = 1 AND ((f.SEND_NAME = :username AND f.RECEIVE_NAME = u.USERNAME) OR (f.SEND_NAME = u.USERNAME AND f.RECEIVE_NAME = :username)) ORDER BY LOWER(u.NAME)";

  $stmt = oci_parse($conn, $query);
  oci_bind_by_name($stmt, ":username", $user);



  oci_execute($stmt);
  $i = 0;
  while (($row = oci_fetch_array($stmt, OCI_ASSOC)) != false){
    $userData[$i] = $row;
    $i++;
   }

  oci_free_statement($stmt);

  // if (empty($userData) && $responseCode == 200){
  //
  //   echo $message;
  //   //echo json_encode($userData);
  //   $responseCode = 410;
  //   $responseString = 'HTTP/1.1 410 Bad Request no user found';
  // }
  // else {
  // }
  echo json_encode($userData);

  header($responseString, true, $responseCode);
  //header('HTTP/1.1 200 Good Request', true, 200);

  oci_close($conn);
?>
