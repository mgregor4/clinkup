<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';


  if (!isset($_GET['username'])) {
    $username = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $username = $_GET['username'];
  }


  $userData = array();
  $query = "SELECT * FROM travels WHERE  username like '{$username}' order by start_date ";
  
  $stmt = oci_parse($conn, $query);

  oci_execute($stmt);
  $i = 0;
  while (($row = oci_fetch_array($stmt, OCI_ASSOC)) != false){
    $userData[$i] = $row;
    $i++;
   }

  oci_free_statement($stmt);
  
  if (empty($userData) && $responseCode == 200){
    $message = 'no user travel';
    echo $message;
    //echo json_encode($userData);
  }
  else {
    echo json_encode($userData);
  }

  header($responseString, true, $responseCode);
  //header('HTTP/1.1 200 Good Request', true, 200);

  oci_close($conn);
?>
