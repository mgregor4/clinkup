<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';

  $postData = file_get_contents("php://input");
  if(isset($postData) && !empty($postData)){
    $request = json_decode($postData);
    $SEND_NAME = trim($request->SEND_NAME);
    $RECEIVE_NAME = trim($request->RECEIVE_NAME);

    $query = "UPDATE messages SET READ = 1 WHERE SEND_NAME = :send AND RECEIVE_NAME = :receive";
    $stmt = oci_parse($conn, $query);
    oci_bind_by_name($stmt, ":status", $STATUS);
    oci_bind_by_name($stmt, ":send", $SEND_NAME);
    oci_bind_by_name($stmt, ":receive", $RECEIVE_NAME);

    $r = oci_execute($stmt);
    if (!$r) {
        $e = oci_error($stmt);  // For oci_execute errors pass the statement handle
        echo htmlentities($e['message']);
        echo htmlentities($e['sqltext']);

      }
      // if(!$r){
      //
      //   $responseCode = 415;
      //   $responseString = 'HTTP/1.1 415 Unable to update status';
      // }
      oci_free_statement($stmt);
      oci_close($conn);

      //$postData[0]['PASSWORD'] = 'encrpyted. not shown';
      //echo json_encode($postData);
    }
    else if (!isset($postData) || empty($postData)){
      $responseCode = 200;
      $responseString = 'HTTP/1.1 200 Bad Request no data';
    }

    header($responseString, true, $responseCode);


?>
