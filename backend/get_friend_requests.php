<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';


  if (!isset($_GET['username'])) {
    $name = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $username = $_GET['username'];
  }


  $friendRequests = array();
  $query = "SELECT f.*, u.USERNAME, u.NAME, u.COMPANY, u.CITY, u.STATE FROM friends f, users u WHERE f.send_name = u.username AND f.RECEIVE_NAME = :username AND f.STATUS < 1 ORDER BY f.STATUS";

  $stmt = oci_parse($conn, $query);
  oci_bind_by_name($stmt, ":username", $username);


  oci_execute($stmt);
  $i = 0;
  while (($row = oci_fetch_array($stmt, OCI_ASSOC)) != false){
    $friendRequests[$i] = $row;
    $i++;
   }

  oci_free_statement($stmt);

  // if (empty($userData) && $responseCode == 200){
  //
  //   echo $message;
  //   //echo json_encode($userData);
  //   $responseCode = 410;
  //   $responseString = 'HTTP/1.1 410 Bad Request no user found';
  // }
  // else {
  // }
  echo json_encode($friendRequests);

  header($responseString, true, $responseCode);
  //header('HTTP/1.1 200 Good Request', true, 200);

  oci_close($conn);
?>
