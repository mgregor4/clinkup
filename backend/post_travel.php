<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';

  $postData = file_get_contents("php://input");

  if(isset($postData) && !empty($postData)){
    $request = json_decode($postData);
    $USERNAME = trim($request->USERNAME);
    $START = trim($request->START_DATE);
    $END = trim($request->END_DATE);
    $CITY = trim($request->CITY);
    $STATE = trim($request->STATE);
    $HOTEL = trim($request->HOTEL);

    // Split Strings
    $SDATE = explode("T",$START);
    $START = $SDATE[0];
    $EDATE = explode("T",$END);
    $END = $EDATE[0];


    $query = "INSERT INTO travels (USERNAME, START_DATE, END_DATE, CITY, STATE, HOTEL)
	VALUES (:username, :startdate, :enddate, :city, :state, :hotel)";

  $query = stripslashes($query);

    $stmt = oci_parse($conn, $query);

    oci_bind_by_name($stmt, ":username", $USERNAME);
    oci_bind_by_name($stmt, ":startdate", $START);
    oci_bind_by_name($stmt, ":enddate", $END);
    oci_bind_by_name($stmt, ":city", $CITY);
    oci_bind_by_name($stmt, ":state", $STATE);
    oci_bind_by_name($stmt, ":hotel", $HOTEL);
    echo $USERNAME;
    oci_execute($stmt);


    oci_free_statement($stmt);
    oci_close($conn);
  }

  //echo $postData;

  header($responseString, true, $responseCode);

?>
