<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';

  $postData = file_get_contents("php://input");
  
  if(isset($postData) && !empty($postData)){
    $request = json_decode($postData);
    $USERNAME = trim($request->USERNAME);
    $PASSWORD = trim($request->PASSWORD);
    $PASSWORD = crypt($PASSWORD, "MPjlw3UNV98wczhg45");
    $NAME = trim($request->UNAME);
    $PRIVACY = trim($request->PRIVACY);
    $CITY = trim($request->CITY);
    $STATE = trim($request->STATE);
    $LINKEDIN = trim($request->LINKEDIN);
    $COMPANY = trim($request->COMPANY);
    $UNIVERSITY = trim($request->UNIVERSITY);
    $BIOGRAPHY = trim($request->BIOGRAPHY);
    $FOODPREF = trim($request->FOODPREF);
    $DIETARY = trim($request->DIETARY);
    $DRINKPREF = trim($request->DRINKPREF);

    $query = "INSERT INTO users (USERNAME, PASSWORD, NAME, PRIVACY, CITY, STATE, LINKEDIN, COMPANY, PHOTO, UNIVERSITY, BIOGRAPHY, FOODPREF, DIETARY, DRINKPREF) VALUES (:uname, :pass, :name, :privacy, :city, :state, :linkedin, :company, :photo, :uni, :bio, :food, :diet, :drink)";
    $query = stripslashes($query);

    $stmt = oci_parse($conn, $query);

    oci_bind_by_name($stmt, ":uname", $USERNAME);
    oci_bind_by_name($stmt, ":pass", $PASSWORD);
    oci_bind_by_name($stmt, ":name", $NAME);
    oci_bind_by_name($stmt, ":privacy", $PRIVACY);
    oci_bind_by_name($stmt, ":city", $CITY);
    oci_bind_by_name($stmt, ":state", $STATE);
    oci_bind_by_name($stmt, ":linkedin", $LINKEDIN);
    oci_bind_by_name($stmt, ":company", $COMPANY);
    oci_bind_by_name($stmt, ":photo", $PHOTO);
    oci_bind_by_name($stmt, ":uni", $UNIVERSITY);
    oci_bind_by_name($stmt, ":bio", $BIOGRAPHY);
    oci_bind_by_name($stmt, ":food", $FOODPREF);
    oci_bind_by_name($stmt, ":diet", $DIETARY);
    oci_bind_by_name($stmt, ":drink", $DRINKPREF);

    oci_execute($stmt);
    $count = oci_num_rows($stmt);
    if($count <1){
      $responseCode = 415;
      $responseString = 'HTTP/1.1 415 Bad Request insert error OR user already exists';
    }
    oci_free_statement($stmt);
    oci_close($conn);
    
  }
  else if (!isset($postData) || empty($postData)){
    $responseCode = 200;
    $responseString = 'HTTP/1.1 200 Bad Request no data';
  }
  
  header($responseString, true, $responseCode);

?>
