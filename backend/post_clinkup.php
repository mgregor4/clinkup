<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';
  echo $postData;
  $postData = file_get_contents("php://input");
  if(isset($postData) && !empty($postData)){
    $request = json_decode($postData);
    $SEND_NAME = trim($request->SEND_NAME);
    $RECEIVE_NAME = trim($request->RECEIVE_NAME);
    $VENUEID = trim($request->VENUEID);
    $DAY = trim($request->DAY);
    $STATUS = trim($request->STATUS);
    $query = "INSERT INTO clinkup (SEND_NAME, RECEIVE_NAME, VENUEID, DAY, STATUS)
      VALUES (:send, :receive, :venueid, :day, :status)";
      $query = stripslashes($query);

        $stmt = oci_parse($conn, $query);

        oci_bind_by_name($stmt, ":send", $SEND_NAME);
        oci_bind_by_name($stmt, ":receive", $RECEIVE_NAME);
        oci_bind_by_name($stmt, ":venueid", $VENUEID);
        oci_bind_by_name($stmt, ":day", $DAY);
        oci_bind_by_name($stmt, ":status", $STATUS);

        oci_execute($stmt);

      $count = oci_num_rows($stmt);
      if($count < 1){
        $responseCode = 415;
        $responseString = 'HTTP/1.1 415 Bad Request insert error';
      }
      oci_free_statement($stmt);
      oci_close($conn);

      //$postData[0]['PASSWORD'] = 'encrpyted. not shown';
      //echo json_encode($postData);
    }
    else if (!isset($postData) || empty($postData)){
      $responseCode = 200;
      $responseString = 'HTTP/1.1 200 Bad Request no data';
    }

    header($responseString, true, $responseCode);


?>
