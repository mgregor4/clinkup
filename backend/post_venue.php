<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';

  $postData = file_get_contents("php://input");

  if(isset($postData) && !empty($postData)){
    $request = json_decode($postData);
    $VENUEID = trim($request->VENUEID);
    $NAME = trim($request->NAME);
    $ADDRESS = trim($request->ADDRESS);
    $PHOTO_URL = trim($request->PHOTO_URL);

    $query = "INSERT INTO venues (VENUEID, NAME, ADDRESS, PHOTO_URL)
      VALUES (:venueid, :name, :address, :photo_url)";
    
    $query = stripslashes($query);
      
      $stmt = oci_parse($conn, $query);

      oci_bind_by_name($stmt, ":venueid", $VENUEID);
      oci_bind_by_name($stmt, ":name", $NAME);
      oci_bind_by_name($stmt, ":address", $ADDRESS);
      oci_bind_by_name($stmt, ":photo_url", $PHOTO_URL);

      oci_execute($stmt);
      $count = oci_num_rows($stmt);
      if($count < 1){
        $responseCode = 415;
        $responseString = 'HTTP/1.1 415 Bad Request insert error';
      }
      oci_free_statement($stmt);
      oci_close($conn);

    }
    else if (!isset($postData) || empty($postData)){
      $responseCode = 200;
      $responseString = 'HTTP/1.1 200 Bad Request no data';
    }

    header($responseString, true, $responseCode);
?>
