<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';


  if (!isset($_GET['name'])) {
    $name = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $name = $_GET['name'];
  }

  if (!isset($_GET['user'])) {
    $user = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $user = $_GET['user'];
  }


  $userData = array();
  if (trim($name)) {
    $query = "SELECT * FROM users WHERE users.username != :username AND (LOWER(name) LIKE LOWER(:name||'%') OR LOWER(city) LIKE LOWER(:name||'%') OR LOWER(company) LIKE LOWER(:name||'%') OR LOWER(university) LIKE LOWER('%'||:name||'%') OR LOWER(username) LIKE LOWER(:name||'%'))";
  } else {
    $query = "SELECT * FROM users WHERE 0 = 1";
  }

  $stmt = oci_parse($conn, $query);
  oci_bind_by_name($stmt, ":username", $user);
  oci_bind_by_name($stmt, ":name", $name);
//   $query = "SELECT * FROM
//     (SELECT u.username, u.name, u.company, u.city, u.state FROM users u WHERE username != '{$user}' AND LOWER(name) LIKE LOWER('{$name}%')) u LEFT JOIN
//     (SELECT * FROM friends f WHERE f.receive_name = '{$user}') f ON u.username = f.send_name
//     UNION
//     SELECT * FROM
//     (SELECT u.username, u.name, u.company, u.city, u.state FROM users u WHERE username != '{$user}' AND LOWER(name) LIKE LOWER('{$name}%')) u LEFT JOIN
//     (SELECT * FROM friends f WHERE f.send_name = '{$user}') f ON u.username = f.receive_name;
// ";


  oci_execute($stmt);
  $i = 0;
  while (($row = oci_fetch_array($stmt, OCI_ASSOC)) != false){
    $userData[$i] = $row;
    $i++;
   }

  oci_free_statement($stmt);

  // if (empty($userData) && $responseCode == 200){
  //
  //   echo $message;
  //   //echo json_encode($userData);
  //   $responseCode = 410;
  //   $responseString = 'HTTP/1.1 410 Bad Request no user found';
  // }
  // else {
  // }
  echo json_encode($userData);

  header($responseString, true, $responseCode);
  //header('HTTP/1.1 200 Good Request', true, 200);

  oci_close($conn);
?>
