<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';


  if (!isset($_GET['send_name'])) {
    $SEND_NAME = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $SEND_NAME = $_GET['send_name'];
  }

  if (!isset($_GET['receive_name'])) {
    $RECEIVE_NAME = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $RECEIVE_NAME = $_GET['receive_name'];
  }

  if (!isset($_GET['offset'])) {
    $OFFSET = 0;
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $OFFSET = $_GET['offset'];
  }


  $messages = array();
  $query = "SELECT SEND_NAME, RECEIVE_NAME, TEXT, READ, to_char(TIMESTAMP - :t_offset/24, 'fmHH:fmMI AM') AS M_TIME FROM messages WHERE (messages.SEND_NAME = :send_name AND messages.RECEIVE_NAME = :receive_name) OR (messages.RECEIVE_NAME = :send_name AND messages.SEND_NAME = :receive_name) ORDER BY messages.timestamp";

  $stmt = oci_parse($conn, $query);

  oci_bind_by_name($stmt, ":send_name", $SEND_NAME);
  oci_bind_by_name($stmt, ":receive_name", $RECEIVE_NAME);
  oci_bind_by_name($stmt, ":t_offset", $OFFSET);

  oci_execute($stmt);

  $i = 0;
  while (($row = oci_fetch_array($stmt, OCI_ASSOC)) != false){
    $messages[$i] = $row;
    $i++;
   }

  oci_free_statement($stmt);

  // if (empty($userData) && $responseCode == 200){
  //
  //   echo $message;
  //   //echo json_encode($userData);
  //   $responseCode = 410;
  //   $responseString = 'HTTP/1.1 410 Bad Request no user found';
  // }
  // else {
  // }
  echo json_encode($messages);

  header($responseString, true, $responseCode);
  //header('HTTP/1.1 200 Good Request', true, 200);

  oci_close($conn);
?>
