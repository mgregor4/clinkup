<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';

  $postData = file_get_contents("php://input");
  if(isset($postData) && !empty($postData)){
    $request = json_decode($postData);
    $SEND_NAME = trim($request->SEND_NAME);
    $RECEIVE_NAME = trim($request->RECEIVE_NAME);
    $TEXT = trim($request->TEXT);

    $query = "INSERT INTO Messages VALUES (:send_name, :receive_name, :text, SYSDATE, 0)";
    $query = stripslashes($query);

    $stmt = oci_parse($conn, $query);

    oci_bind_by_name($stmt, ":send_name", $SEND_NAME);
    oci_bind_by_name($stmt, ":receive_name", $RECEIVE_NAME);
    oci_bind_by_name($stmt, ":text", $TEXT);

    // $query = "INSERT INTO users (USERNAME, PASSWORD, NAME, PRIVACY, CITY, STATE, LINKEDIN, COMPANY, UNIVERSITY, BIOGRAPHY, FOODPREF, DIETARY, DRINKPREF) VALUES ('{$USERNAME}','{$PASSWORD}', '{$NAME}', {$PRIVACY}, '{$CITY}', '{$STATE}', '{$LINKEDIN}', '{$COMPANY}', '{$UNIVERSITY}', '{$BIOGRAPHY}', '{$FOODPREF}', '{$DIETARY}', '{$DRINKPREF}')";

    // $stmt = oci_parse($conn, $query);
    echo $stmt;
    oci_execute($stmt);
    $e = oci_error($stmt);
    echo json_encode($e);
    $count = oci_num_rows($stmt);
    if($count <1){
      $responseCode = 415;
      $responseString = 'HTTP/1.1 415 Bad Request insert error OR user already exists';
    }
    oci_free_statement($stmt);
    oci_close($conn);

    //$postData[0]['PASSWORD'] = 'encrpyted. not shown';
    //echo json_encode($postData);
  }
  else if (!isset($postData) || empty($postData)){
    $responseCode = 200;
    $responseString = 'HTTP/1.1 200 Bad Request no data';
  }

  header($responseString, true, $responseCode);

?>
