<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';


  if (!isset($_GET['SEND_NAME']) || !isset($_GET['RECEIVE_NAME'])) {
    $username = 'incomplete args';
    $password = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $SEND_NAME = $_GET['SEND_NAME'];
    $RECEIVE_NAME = $_GET['RECEIVE_NAME'];
  }

  $friendData = array();

  $query = "SELECT * FROM friends WHERE  (SEND_NAME like :send_name and RECEIVE_NAME like :receive_name) or (SEND_NAME like :receive_name and RECEIVE_NAME like :send_name) and ROWNUM <= 1";
  $query = stripslashes($query);

  $stmt = oci_parse($conn, $query);

  oci_bind_by_name($stmt, ":send_name", $SEND_NAME);
  oci_bind_by_name($stmt, ":receive_name", $RECEIVE_NAME);

  oci_execute($stmt);
  $i = 0;
  while (($row = oci_fetch_array($stmt, OCI_ASSOC)) != false){
    $friendData[$i] = $row;
    $i++;
   }

  oci_free_statement($stmt);

  if (empty($friendData) && $responseCode == 200){
    $responseCode = 200;
    echo json_encode($friendData[0]);
    $responseString = 'HTTP/1.1 200 Good Request no friendship found';
  }
  else {
    echo json_encode($friendData[0]);
  }

  header($responseString, true, $responseCode);

  oci_close($conn);
?>
