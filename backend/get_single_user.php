<?php
  require 'database.php';

  $responseCode = 200;
  $responseString = 'HTTP/1.1 200 Good Request';


  if (!isset($_GET['username']) || !isset($_GET['password'])) {
    $username = 'incomplete args';
    $password = 'incomplete args';
    $responseCode = 420;
    $responseString = 'HTTP/1.1 420 Bad Request invalid input';
  }
  else {
    $username = $_GET['username'];
    $password = $_GET['password'];
    $password = crypt($password, "MPjlw3UNV98wczhg45");
  }

  $userData = array();
  $query = "SELECT * FROM users WHERE  username like :uname and ROWNUM <= 1";
  $query = stripslashes($query);

  $stmt = oci_parse($conn, $query);

  oci_bind_by_name($stmt, ":uname", $username);

  oci_execute($stmt);
  $i = 0;
  while (($row = oci_fetch_array($stmt, OCI_ASSOC)) != false){
    $userData[$i] = $row;
    $i++;
   }

  oci_free_statement($stmt);

  if (empty($userData) && $responseCode == 200){
    $message = 'no user';
    echo $message;
    $responseCode = 410;
    $responseString = 'HTTP/1.1 410 Bad Request no user found';
  }
  else if ($userData[0]['PASSWORD'] !== $password  && $responseCode == 200){
    $responseCode = 415;
    $responseString = 'HTTP/1.1 415 Bad Request incorrect password';
  }
  else {
    $userData[0]['PASSWORD'] = 'encrypted. not shown';
    echo json_encode($userData[0]);
  }

  header($responseString, true, $responseCode);

  oci_close($conn);
?>
