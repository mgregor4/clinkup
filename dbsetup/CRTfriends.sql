drop table friends;

create table friends
	(send_name varchar(20),
	receive_name varchar(20),
	start_date date,
	status int,
		foreign key (send_name) references users(username) on delete cascade,
		foreign key (receive_name) references users(username) on delete cascade,
		constraint friends_pk primary key (send_name, receive_name)
	);
exit;
