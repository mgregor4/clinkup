drop table clinkup;

create table clinkup
	(send_name varchar(20),
	receive_name varchar(20),
	venueid varchar(255),
	day varchar(50),
	status int,
		foreign key (send_name) references users(username) on delete cascade,
		foreign key (receive_name) references users(username) on delete cascade,
		foreign key (venueid) references venues(venueid) on delete cascade,
		constraint clinkup_pk primary key (send_name, receive_name, venueid, day)
	);
exit;
