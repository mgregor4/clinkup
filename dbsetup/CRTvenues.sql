drop table venues cascade constraints;

create table venues
	(venueid varchar(255) primary key,
	name varchar(255) not null,
	address varchar(255),
	photo_url varchar(255)
	);
exit;
	
