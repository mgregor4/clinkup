drop table users cascade constraints;

create table users
	(username varchar(20) primary key,
	password varchar(100) not null,
	name varchar(50) not null,
	city varchar(50),
	state varchar(20),
	linkedin varchar(50),
	company varchar(255),
	privacy int not null,
	photo varchar(200),
	university varchar(100),
	biography varchar(255),
	foodpref varchar(100),
	drinkpref varchar(100),
	dietary varchar(100)
	);
exit;
	
