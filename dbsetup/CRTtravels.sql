drop table travels;

create table travels
	(username varchar(20),
	start_date varchar(50),
	end_date varchar(50),
	city varchar(50),
	state varchar(20),
	hotel varchar(255),
		foreign key (username) references users(username) on delete cascade,
		constraint travels_pk primary key (username, start_date, end_date)
		);
exit;
