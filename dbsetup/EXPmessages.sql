--set colsep ','
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 2000
set feedback off
--set numwidth 5
set markup html preformat on

spool messages.csv

select send_name || ',' || receive_name || ',' || replace(text,',','') || ',' || timestamp || ',' || read
from messages;

spool off

exit;

