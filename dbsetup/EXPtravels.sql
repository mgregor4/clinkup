--set colsep ','
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 2000
set feedback off
--set numwidth 5
set markup html preformat on

spool travels.csv

select username || ',' || start_date || ',' || end_date || ',' || city || ',' || state || ',' || hotel from travels;

spool off

exit;

