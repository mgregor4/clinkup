--set colsep ','
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 2000
set feedback off
--set numwidth 5
set markup html preformat on

spool venues.csv

select venueid || ',' || name || ',' || address || ',' || photo_url from venues;

spool off

exit;

