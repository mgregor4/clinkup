--set colsep ','
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 2000
set feedback off
--set numwidth 5
set markup html preformat on

spool users.csv

select username || ',' || password || ',' || name || ',' || city || ',' || state
|| ',' || linkedin || ',' || company || ',' || privacy || ',' || photo || ',' || 
university || ',' || biography || ',' || foodpref || ',' || drinkpref || ',' ||
dietary from users;

spool off

exit;

