--set colsep ','
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 2000
set feedback off
--set numwidth 5
set markup html preformat on

spool friends.csv

select send_name || ',' || receive_name || ',' || start_date || ',' || status from friends;

spool off

exit;

