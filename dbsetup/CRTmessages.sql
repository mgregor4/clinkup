drop table messages;

create table messages
	(send_name varchar(20),
	receive_name varchar(20),
	text varchar(255),
	timestamp TIMESTAMP,
	READ number(1),
		foreign key(send_name) references users(username) on delete cascade,
		foreign key(receive_name) references users(username) on delete cascade,
		constraint messages_pk primary key (send_name, receive_name, timestamp)
	);
exit;
